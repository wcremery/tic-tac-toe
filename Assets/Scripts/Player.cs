﻿using System.Collections;
using System.Collections.Generic;

public class Player
{
    private int idPlayer;
    private string sign;
    private int score;
    private string name;

    public int IdPlayer { get => idPlayer; set => idPlayer = value; }
    public string Sign { get => sign; }
    public int Score { get => score; set => score = value; }
    public string Name { get => name; set => name = value; }            

    public Player(int idPlayer)
    {
        this.idPlayer = idPlayer;
        this.sign = idPlayer == 1 ? "X" : "O";
        this.score = 0;
    }
}
