﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

using DG.Tweening;

public class Box : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    private Toggle myToggle;
    private Text playerSign;
    private GameplayController gameplayController;

    void Awake()
    {
        gameplayController = GameObject.Find("GameplayController").GetComponent<GameplayController>();
    }
    // Start is called before the first frame update
    void Start()
    {        
        myToggle = GetComponent<Toggle>();
        playerSign = GetComponent<Text>();
        playerSign.color = Color.clear;
    }

    public void OnPointerClick(PointerEventData eventData)
    {       
        if (myToggle.interactable)
        {
            myToggle.interactable = !myToggle.interactable;
            playerSign.color = (gameplayController.CurrentPlayer.Sign.Equals("X")) ? new Color(0, 206, 209) : new Color(209, 0, 206);
            if (gameplayController.CurrentPlayer.Sign.Equals("X")) gameplayController.AudioSourcePlayerOne.Play();
            else gameplayController.AudioSourcePlayerTwo.Play();
            myToggle.transform.DOScale(1.5F, 1);            
            gameplayController.EndTurn(GetLine(), GetColumn());    
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (myToggle.interactable)
        {
            myToggle.transform.localScale = new Vector3(1, 1, 1);
            playerSign.text = gameplayController.CurrentPlayer.Sign;
            playerSign.color = Color.grey;            
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (myToggle.interactable) playerSign.color = Color.clear;
    }


    internal int GetColumn()
    {
        float posX = playerSign.rectTransform.localPosition.x;

        if (posX > 0)
        {
            return 3;
        }
        else if (posX < 0)
        {
            return 1;
        }
        else
        {
            return 2;
        }
    }

    internal int GetLine()
    {
        float posY = playerSign.rectTransform.localPosition.y;

        if (posY > 0)
        {
            return 1;
        }
        else if (posY < 0)
        {
            return 3;
        }
        else
        {
            return 2;
        }

    }
}
