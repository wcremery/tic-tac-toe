﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BackButton : MonoBehaviour
{
    private Button backButton;

    // Start is called before the first frame update
    void Start()
    {
        backButton = transform.Find("Back").GetComponent<Button>();
        backButton.onClick.AddListener(delegate { SceneManager.LoadScene("TitleScreen"); });
    }
}
