﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartButton : MonoBehaviour
{
    private Button pviaButton;
    private Button pvpButton;
    private static int gameMode;

    public static int GameMode { get => gameMode; }

    // Start is called before the first frame update
    void Start()
    {
        pviaButton = transform.Find("PvIAButton").GetComponent<Button>();
        pvpButton = transform.Find("PvPButton").GetComponent<Button>();
        pviaButton.onClick.AddListener(delegate { StartCoroutine(LoadMainScene()); gameMode = 1; });
        pvpButton.onClick.AddListener(delegate { StartCoroutine(LoadMainScene()); gameMode = 2; });
    }

    private IEnumerator LoadMainScene()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Main");
        while(!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
