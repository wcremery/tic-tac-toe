﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EntriesManager : MonoBehaviour
{
    private Button applyButton;   
    private TextMeshProUGUI playerOneEntry;
    private TextMeshProUGUI playerTwoEntry;
    private GameplayController gameplayController;
    private Score scoreComponent;
    private string p1name;
    private string p2name;

    // Start is called before the first frame update
    void Start()
    {
        gameplayController = GameObject.Find("GameplayController").GetComponent<GameplayController>();
        scoreComponent = GameObject.Find("Score").GetComponent<Score>();
        applyButton = GameObject.Find("ApplyButton").GetComponent<Button>();
        applyButton.onClick.AddListener(delegate { UpdatePlayersNames(); });
        playerOneEntry = GameObject.Find("TextP1").GetComponent<TextMeshProUGUI>();
        if (gameplayController.GameMode == 1) GameObject.Find("PlayerTwoEntry").SetActive(false);
        else playerTwoEntry = GameObject.Find("TextP2").GetComponent<TextMeshProUGUI>();               
    }

    private void UpdatePlayersNames()
    {
        p1name = playerOneEntry.text;
        p2name = (gameplayController.GameMode == 1) ? "IA" : playerTwoEntry.text;
        scoreComponent.UpdateDisplayedNames(p1name, p2name);
    }
}
