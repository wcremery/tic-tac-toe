﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridEffect : MonoBehaviour
{
    private RectTransform panel;
    private RectTransform canvas;
    private RectTransform[] grids;

    // Start is called before the first frame update
    void Start()
    {
        panel = transform.parent.parent.Find("Board").GetComponent<RectTransform>();
        canvas = panel.GetComponentInParent<RectTransform>();
        grids = GetComponentsInChildren<RectTransform>();
        float panelLength;
        float endPos;
        float speedAnim = 1.5F;

        foreach (RectTransform grid in grids)
        {            
            if (grid.localPosition.x < 0)
            {
                panelLength = panel.rect.width;
                endPos = canvas.position.x - ((panelLength / 3) / 2);                
                grid.transform.DOMoveX(endPos, speedAnim);
            }
            if (grid.localPosition.x > 0)
            {
                panelLength = panel.rect.width;
                endPos = canvas.position.x + ((panelLength / 3) / 2);
                grid.transform.DOMoveX(endPos, speedAnim);
            }
            if (grid.localPosition.y < 0)
            {
                panelLength = panel.rect.height;
                endPos = canvas.position.y - ((panelLength / 3) / 2);
                grid.transform.DOMoveY(endPos, speedAnim);
            }
            if (grid.localPosition.y > 0)
            {
                panelLength = panel.rect.height;
                endPos = canvas.position.y + ((panelLength / 3) / 2);
                grid.transform.DOMoveY(endPos, speedAnim);
            }
        }    
        
    }
}
