﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameplayController : MonoBehaviour
{
    private Player p1 = new Player(1);
    private Player p2 = new Player(2);
    private Player currentPlayer;
    private Score score;
    private int gameMode;

    private List<List<string>> board = new List<List<string>>();
    private Toggle[] toggles;
    private Text[] playerSignContainers;    
    private Button restartButton;

    private AudioSource audioSourcePlayerOne;
    private AudioSource audioSourcePlayerTwo;

    private List<string> l1 = new List<string>();
    private List<string> l2 = new List<string>();
    private List<string> l3 = new List<string>();
    private List<string> c1 = new List<string>();
    private List<string> c2 = new List<string>();
    private List<string> c3 = new List<string>();
    private List<string> d1 = new List<string>();
    private List<string> d2 = new List<string>();    

    public Player CurrentPlayer { get => currentPlayer; set => currentPlayer = value; }
    public Player P1 { get => p1; set => p1 = value; }
    public Player P2 { get => p2; set => p2 = value; }
    public int GameMode { get => gameMode; set => gameMode = value; }
    public AudioSource AudioSourcePlayerOne { get => audioSourcePlayerOne; set => audioSourcePlayerOne = value; }
    public AudioSource AudioSourcePlayerTwo { get => audioSourcePlayerTwo; set => audioSourcePlayerTwo = value; }


    // Start is called before the first frame update
    void Start()
    {
        gameMode = StartButton.GameMode;  
        score = GameObject.Find("Score").GetComponent<Score>();
        toggles = GameObject.Find("Boxes").GetComponentsInChildren<Toggle>();
        playerSignContainers = GameObject.Find("Boxes").GetComponentsInChildren<Text>();
        restartButton = GameObject.Find("Restart").GetComponent<Button>();
        restartButton.onClick.AddListener(delegate { Initialization(); });
        currentPlayer = p1;

        audioSourcePlayerOne = GameObject.Find("PlayerOneSound").GetComponent<AudioSource>();
        audioSourcePlayerTwo = GameObject.Find("PlayerTwoSound").GetComponent<AudioSource>();
        
        board.Add(l1);
        board.Add(l2);
        board.Add(l3);
        board.Add(c1);
        board.Add(c2);
        board.Add(c3);
        board.Add(d1);
        board.Add(d2);        
    }

    public void Initialization()
    {
        currentPlayer = p1;
        // clear list containing players entries
        foreach(List<string> list in board)
        {
            list.Clear();
        }

        // make boxes all interactible again
        foreach(Toggle toggle in toggles)
        {
            toggle.interactable = true;
            toggle.transform.localScale = new Vector3(1, 1, 1);
        }

        // clear player sign in all boxes
        foreach(Text playerSign in playerSignContainers)
        {
            playerSign.text = "";            
        }
        
    }

    internal void EndTurn(int posX, int posY)
    {
        bool isFinished = false;

        if (posX == 1) l1.Add(currentPlayer.Sign);
        else if (posX == 2) l2.Add(currentPlayer.Sign);
        else l3.Add(currentPlayer.Sign);

        if (posY == 1) c1.Add(currentPlayer.Sign);
        else if (posY == 2) c2.Add(currentPlayer.Sign);
        else c3.Add(currentPlayer.Sign);

        if (posX == 1 && posY == 1) d1.Add(currentPlayer.Sign);
        else if (posX == 2 && posY == 2) d1.Add(currentPlayer.Sign);
        else if (posX == 3 && posY == 3) d1.Add(currentPlayer.Sign);

        if (posX == 1 && posY == 3) d2.Add(currentPlayer.Sign);
        else if (posX == 2 && posY == 2) d2.Add(currentPlayer.Sign);
        else if (posX == 3 && posY == 1) d2.Add(currentPlayer.Sign);        

        if (IsGameOver())
        {
            isFinished = true;
            Debug.LogError("GAME OVER !");            
            foreach(Toggle currentToggle in toggles)
            {
                if (currentToggle.interactable) currentToggle.interactable = false;
            }
            score.ScoreUpdate(p1.Score, p2.Score);
        }        
        else
        {
            currentPlayer = (currentPlayer == p1) ? p2 : p1;
        }

        if (!isFinished && gameMode == 1 && currentPlayer == p2)
        {
            List<Toggle> interactibleToggles = new List<Toggle>();
            foreach (Toggle currentToggle in toggles)
            {
                if (currentToggle.interactable) interactibleToggles.Add(currentToggle);
            }
            int iaChoice = Random.Range(0, interactibleToggles.Count - 1);            
            int xIA = (int)interactibleToggles[iaChoice].GetComponent<RectTransform>().localPosition.x;
            int yIA = (int)interactibleToggles[iaChoice].GetComponent<RectTransform>().localPosition.y;
            int posXIA, posYIA;
            if (xIA > 0)
            {
                posXIA = 3;
            }
            else if (xIA < 0)
            {
                posXIA = 1;
            }
            else
            {
                posXIA = 2;
            }            

            if (yIA > 0)
            {
                posYIA = 1;
            }
            else if (yIA < 0)
            {
                posYIA = 3;
            }
            else
            {
                posYIA = 2;
            }

            interactibleToggles[iaChoice].GetComponent<Text>().text = currentPlayer.Sign;
            interactibleToggles[iaChoice].GetComponent<Text>().color = new Color(209, 0, 206);
            interactibleToggles[iaChoice].GetComponentInParent<Box>().transform.DOScale(1.5F, 1);
            interactibleToggles[iaChoice].interactable = false;

            EndTurn(posYIA, posXIA);
        }
    }

    private bool IsGameOver()
    {
        foreach (List<string> playerEntry in board)
        {
            
            if (playerEntry.Count == 3)
            {
                bool diffFound = false;
                foreach (string valueInBox in playerEntry)
                {
                    if (!valueInBox.Equals(currentPlayer.Sign)) diffFound = true;
                }
                if (diffFound == false)
                {
                    Debug.LogError("Player " + currentPlayer.IdPlayer + " wins");
                    currentPlayer.Score++;
                    return true;
                }
            }            
        }

        return false;
    }
}