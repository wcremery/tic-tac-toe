﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    private TextMeshProUGUI scores;
    private GameplayController gameplayController;
    private string namePlayer1;
    private string namePlayer2;

    public string NamePlayer1 { get => namePlayer1; set => namePlayer1 = value; }
    public string NamePlayer2 { get => namePlayer2; set => namePlayer2 = value; }

    // Start is called before the first frame update
    void Start()
    {
        gameplayController = GameObject.Find("GameplayController").GetComponent<GameplayController>();
        scores = transform.Find("scores").GetComponent<TextMeshProUGUI>();
        namePlayer1 = "P1";
        namePlayer2 = (gameplayController.GameMode == 1) ? "IA" : "P2";
        scores.SetText(namePlayer1 + " : 0\n" + namePlayer2 + " : 0");
    }

    public void ScoreUpdate(int p1Score, int p2Score)
    {
        scores.SetText(namePlayer1 + " : "+ gameplayController.P1.Score + "\n" + namePlayer2 + " : " + gameplayController.P2.Score);
    }

    public void UpdateDisplayedNames(string nameP1, string nameP2)
    {
        namePlayer1 = nameP1;
        namePlayer2 = nameP2;
        scores.SetText(namePlayer1 + " : 0\n" + namePlayer2 + " : 0");
        gameplayController.P1.Score = 0;
        gameplayController.P2.Score = 0;
        gameplayController.Initialization();
    }
}
