﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void BackButton::Start()
extern void BackButton_Start_mED8FAF7A752B3AD143F9F0C563F82CD1F13C7194 (void);
// 0x00000002 System.Void BackButton::.ctor()
extern void BackButton__ctor_m7F3E5F7BC80AEB42857907CEEB4E70C95F7ADD74 (void);
// 0x00000003 System.Void BackButton_<>c::.cctor()
extern void U3CU3Ec__cctor_m777FC6B3355E4BE0CF900D7BC9FAAFCDA5D75C50 (void);
// 0x00000004 System.Void BackButton_<>c::.ctor()
extern void U3CU3Ec__ctor_mE52D3914667235EC01164521FDEFC27582585D57 (void);
// 0x00000005 System.Void BackButton_<>c::<Start>b__1_0()
extern void U3CU3Ec_U3CStartU3Eb__1_0_mCFB87C3E8101789B3DE2882E7588033AFC711257 (void);
// 0x00000006 System.Void Box::Awake()
extern void Box_Awake_m6804B828DFC994FBB4B5F7633C167831486AAEA0 (void);
// 0x00000007 System.Void Box::Start()
extern void Box_Start_m303F32963DB378B8D1424990FB77B8667550EB60 (void);
// 0x00000008 System.Void Box::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void Box_OnPointerClick_m7C9A78825F697B96F4FBB098D454056869BD9D1D (void);
// 0x00000009 System.Void Box::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void Box_OnPointerEnter_m73280582AB84000A80E302D85CA4E3374F363625 (void);
// 0x0000000A System.Void Box::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void Box_OnPointerExit_m4A7036E9052550B7C0639EF75C2E1C062789851C (void);
// 0x0000000B System.Int32 Box::GetColumn()
extern void Box_GetColumn_m836860B15F42088D3245D2C65AA3FF02EFDC3080 (void);
// 0x0000000C System.Int32 Box::GetLine()
extern void Box_GetLine_m00CE6139F0ECCB8ED23D4C29AF8B7D50EEA6813F (void);
// 0x0000000D System.Void Box::.ctor()
extern void Box__ctor_mFDB4D9912FDCE47BA2A62DB77AD92F62989E0295 (void);
// 0x0000000E System.Void EntriesManager::Start()
extern void EntriesManager_Start_m53F5C106BC3BC8294A14DB225E5179356C62378F (void);
// 0x0000000F System.Void EntriesManager::UpdatePlayersNames()
extern void EntriesManager_UpdatePlayersNames_m37FE34EBC83D16045DC5435D1F03191515543D50 (void);
// 0x00000010 System.Void EntriesManager::.ctor()
extern void EntriesManager__ctor_m66B0155629BDFEA05CC6FBE07A7B5FB6B57A5B7C (void);
// 0x00000011 System.Void EntriesManager::<Start>b__7_0()
extern void EntriesManager_U3CStartU3Eb__7_0_m13A7D85BF8A16BC5E04F913612EC66A365068DFB (void);
// 0x00000012 Player GameplayController::get_CurrentPlayer()
extern void GameplayController_get_CurrentPlayer_m64DF09B483531D2BC8638B3A7EB6BCF92DDA6BBF (void);
// 0x00000013 System.Void GameplayController::set_CurrentPlayer(Player)
extern void GameplayController_set_CurrentPlayer_m23C87603DDF92A9D64EE179D545B034A63E1E9ED (void);
// 0x00000014 Player GameplayController::get_P1()
extern void GameplayController_get_P1_m4B6EA2B345DFBD9100D07F604349BB972E850A28 (void);
// 0x00000015 System.Void GameplayController::set_P1(Player)
extern void GameplayController_set_P1_m31F415A90BA634193384075424AF60ADDFDAF558 (void);
// 0x00000016 Player GameplayController::get_P2()
extern void GameplayController_get_P2_m7C6E95B4966050C54211A66121ADFC88FCC06698 (void);
// 0x00000017 System.Void GameplayController::set_P2(Player)
extern void GameplayController_set_P2_m28254E7140A52B43190FE4E794C5FA7836C2A5E6 (void);
// 0x00000018 System.Int32 GameplayController::get_GameMode()
extern void GameplayController_get_GameMode_m25FA4D34FA402DEA22F8F9053DE278F2538BBFF6 (void);
// 0x00000019 System.Void GameplayController::set_GameMode(System.Int32)
extern void GameplayController_set_GameMode_m02060946BF0F509B79D717DE7E7035C7FD21544B (void);
// 0x0000001A UnityEngine.AudioSource GameplayController::get_AudioSourcePlayerOne()
extern void GameplayController_get_AudioSourcePlayerOne_m4F0007CE23F636247A334B79305ABE9501245DFF (void);
// 0x0000001B System.Void GameplayController::set_AudioSourcePlayerOne(UnityEngine.AudioSource)
extern void GameplayController_set_AudioSourcePlayerOne_m5ED5E3B95B1BF8C3668D11AAB59643982BD8E8BC (void);
// 0x0000001C UnityEngine.AudioSource GameplayController::get_AudioSourcePlayerTwo()
extern void GameplayController_get_AudioSourcePlayerTwo_mC83C9D57B149A3F3C55C72BEFC405E43DFDFB431 (void);
// 0x0000001D System.Void GameplayController::set_AudioSourcePlayerTwo(UnityEngine.AudioSource)
extern void GameplayController_set_AudioSourcePlayerTwo_m38B3D5676DA9A299AA3652EF490ACBDFE4DF0EE8 (void);
// 0x0000001E System.Void GameplayController::Start()
extern void GameplayController_Start_m00F87B1BD1027249FB804267BEF17AC4E5D4BC08 (void);
// 0x0000001F System.Void GameplayController::Initialization()
extern void GameplayController_Initialization_m33290712194E9FF2663E23116A4E5F80D36964DE (void);
// 0x00000020 System.Void GameplayController::EndTurn(System.Int32,System.Int32)
extern void GameplayController_EndTurn_m00C5D392D0EC03867821D2EA7E48460F5F135642 (void);
// 0x00000021 System.Boolean GameplayController::IsGameOver()
extern void GameplayController_IsGameOver_m4A5FFE2AB7F518192334A9F33EAC7BCBAFFCE7FA (void);
// 0x00000022 System.Void GameplayController::.ctor()
extern void GameplayController__ctor_m6E7B42579EA00C13984BF45D41FDDCA3C58916F6 (void);
// 0x00000023 System.Void GameplayController::<Start>b__37_0()
extern void GameplayController_U3CStartU3Eb__37_0_mA35BB044D7E6E063A38B5C9EB34C2D6C9477E4A1 (void);
// 0x00000024 System.Void GridEffect::Start()
extern void GridEffect_Start_m2D58A6254DC64C552C0CBF46FF7DE0F94787858F (void);
// 0x00000025 System.Void GridEffect::.ctor()
extern void GridEffect__ctor_mB7B3CFA091AE49B9CE9AF379FBC3E027BCFC92EB (void);
// 0x00000026 System.Int32 Player::get_IdPlayer()
extern void Player_get_IdPlayer_mBD433887F4B838E2FC35BEE4AE32EFE0B3AB6FF6 (void);
// 0x00000027 System.Void Player::set_IdPlayer(System.Int32)
extern void Player_set_IdPlayer_m1CF2961F80F874ADE45749BECFA2F7057F09F4AA (void);
// 0x00000028 System.String Player::get_Sign()
extern void Player_get_Sign_mEA0B0257A2DCC18B4E3357DE7689841E0FAF677E (void);
// 0x00000029 System.Int32 Player::get_Score()
extern void Player_get_Score_mAD918B21E96F09C922D7D98E9F76FBD3505960B0 (void);
// 0x0000002A System.Void Player::set_Score(System.Int32)
extern void Player_set_Score_m3553298246BA3DB3698209F34278DB2D752427CC (void);
// 0x0000002B System.String Player::get_Name()
extern void Player_get_Name_m759A894136CA994C1A36BBD5E10F8D6FDF488BE8 (void);
// 0x0000002C System.Void Player::set_Name(System.String)
extern void Player_set_Name_mF5B95A6563BFFB722973440BFA83AE2EE1DCF41C (void);
// 0x0000002D System.Void Player::.ctor(System.Int32)
extern void Player__ctor_mD67D4225E11A8F52CC815E852AB9F677F93E59CB (void);
// 0x0000002E System.String Score::get_NamePlayer1()
extern void Score_get_NamePlayer1_m4C79819FBFEC9B0AD22A8A1CE9D712537E243BCE (void);
// 0x0000002F System.Void Score::set_NamePlayer1(System.String)
extern void Score_set_NamePlayer1_m19613351986F26A3C04678707CF45CC42DB1EC25 (void);
// 0x00000030 System.String Score::get_NamePlayer2()
extern void Score_get_NamePlayer2_m532B35B0A10FA2A91B5830181401C36251B272E5 (void);
// 0x00000031 System.Void Score::set_NamePlayer2(System.String)
extern void Score_set_NamePlayer2_m0FB48D3C338FFE5327011615C400A2D761A1A3C5 (void);
// 0x00000032 System.Void Score::Start()
extern void Score_Start_m65B0DAC30D1A9E246D4CF14672D8138B53EDA564 (void);
// 0x00000033 System.Void Score::ScoreUpdate(System.Int32,System.Int32)
extern void Score_ScoreUpdate_m60681E317A27078F33EB487E7DD61DE167EEEA97 (void);
// 0x00000034 System.Void Score::UpdateDisplayedNames(System.String,System.String)
extern void Score_UpdateDisplayedNames_mD5E2346E01EAB7F4C0D301C940E52726E3393E33 (void);
// 0x00000035 System.Void Score::.ctor()
extern void Score__ctor_mEE9186D20D9B28A735262B29AB6E8D9FF1380FB6 (void);
// 0x00000036 System.Int32 StartButton::get_GameMode()
extern void StartButton_get_GameMode_m98C915DDB3138681442B6E8B57371DF6ACDFF27D (void);
// 0x00000037 System.Void StartButton::Start()
extern void StartButton_Start_m73507426F11BB8A54FE7939A7CBC7D4D0F78E27E (void);
// 0x00000038 System.Collections.IEnumerator StartButton::LoadMainScene()
extern void StartButton_LoadMainScene_m262E9A17B5D5530BA6ED4502B1C69F5396DAE9B9 (void);
// 0x00000039 System.Void StartButton::.ctor()
extern void StartButton__ctor_m6DAB2093EB360099E446CA6B746441526C0C7BD9 (void);
// 0x0000003A System.Void StartButton::<Start>b__5_0()
extern void StartButton_U3CStartU3Eb__5_0_m39E06A9C5B023E0C5092F1B00C1BD84223983715 (void);
// 0x0000003B System.Void StartButton::<Start>b__5_1()
extern void StartButton_U3CStartU3Eb__5_1_m024F8B089B8F8B4F3747B1CF5B7BE99DB88580DD (void);
// 0x0000003C System.Void StartButton_<LoadMainScene>d__6::.ctor(System.Int32)
extern void U3CLoadMainSceneU3Ed__6__ctor_m9ED86CF7C2035A18EDE99E643B6EBF661738CB02 (void);
// 0x0000003D System.Void StartButton_<LoadMainScene>d__6::System.IDisposable.Dispose()
extern void U3CLoadMainSceneU3Ed__6_System_IDisposable_Dispose_m99CAEF5C5CD4D205BF82D247C097D399BB49D8DA (void);
// 0x0000003E System.Boolean StartButton_<LoadMainScene>d__6::MoveNext()
extern void U3CLoadMainSceneU3Ed__6_MoveNext_mE08BEA0C1051A2019F7F3BF9A09F215811E5E408 (void);
// 0x0000003F System.Object StartButton_<LoadMainScene>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadMainSceneU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9E12BD1757333BC11838D94206F57B95C3B9BC3E (void);
// 0x00000040 System.Void StartButton_<LoadMainScene>d__6::System.Collections.IEnumerator.Reset()
extern void U3CLoadMainSceneU3Ed__6_System_Collections_IEnumerator_Reset_m78D8FDD7095F220C7657A3CB7474666B317DF797 (void);
// 0x00000041 System.Object StartButton_<LoadMainScene>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CLoadMainSceneU3Ed__6_System_Collections_IEnumerator_get_Current_m5083C604160556C02DBAEDD48AA9AA3953343B13 (void);
// 0x00000042 System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2 (void);
// 0x00000043 System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B (void);
// 0x00000044 System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371 (void);
// 0x00000045 System.Void ChatController::.ctor()
extern void ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026 (void);
// 0x00000046 System.Void DropdownSample::OnButtonClick()
extern void DropdownSample_OnButtonClick_m97641451A2EAC4FC6682897A948A37FDD6BF9EA6 (void);
// 0x00000047 System.Void DropdownSample::.ctor()
extern void DropdownSample__ctor_m87664E3DC75A1A9AA19FEE6ED6B993AB1E50E2B9 (void);
// 0x00000048 System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93 (void);
// 0x00000049 System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE (void);
// 0x0000004A System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC (void);
// 0x0000004B System.Void EnvMapAnimator_<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23 (void);
// 0x0000004C System.Void EnvMapAnimator_<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B (void);
// 0x0000004D System.Boolean EnvMapAnimator_<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE (void);
// 0x0000004E System.Object EnvMapAnimator_<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E (void);
// 0x0000004F System.Void EnvMapAnimator_<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB (void);
// 0x00000050 System.Object EnvMapAnimator_<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF (void);
// 0x00000051 System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226 (void);
// 0x00000052 System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E (void);
// 0x00000053 System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1 (void);
// 0x00000054 System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3 (void);
// 0x00000055 TMPro.TMP_TextEventHandler_CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385 (void);
// 0x00000056 System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler_CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4 (void);
// 0x00000057 TMPro.TMP_TextEventHandler_SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B (void);
// 0x00000058 System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler_SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788 (void);
// 0x00000059 TMPro.TMP_TextEventHandler_WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A (void);
// 0x0000005A System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler_WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076 (void);
// 0x0000005B TMPro.TMP_TextEventHandler_LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D (void);
// 0x0000005C System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler_LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA (void);
// 0x0000005D TMPro.TMP_TextEventHandler_LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB (void);
// 0x0000005E System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler_LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5 (void);
// 0x0000005F System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70 (void);
// 0x00000060 System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11 (void);
// 0x00000061 System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC (void);
// 0x00000062 System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44 (void);
// 0x00000063 System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC (void);
// 0x00000064 System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53 (void);
// 0x00000065 System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66 (void);
// 0x00000066 System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43 (void);
// 0x00000067 System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77 (void);
// 0x00000068 System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8 (void);
// 0x00000069 System.Void TMPro.TMP_TextEventHandler_CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7 (void);
// 0x0000006A System.Void TMPro.TMP_TextEventHandler_SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2 (void);
// 0x0000006B System.Void TMPro.TMP_TextEventHandler_WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C (void);
// 0x0000006C System.Void TMPro.TMP_TextEventHandler_LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018 (void);
// 0x0000006D System.Void TMPro.TMP_TextEventHandler_LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0 (void);
// 0x0000006E System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C (void);
// 0x0000006F System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F (void);
// 0x00000070 System.Void TMPro.Examples.Benchmark01_<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB (void);
// 0x00000071 System.Void TMPro.Examples.Benchmark01_<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73 (void);
// 0x00000072 System.Boolean TMPro.Examples.Benchmark01_<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C (void);
// 0x00000073 System.Object TMPro.Examples.Benchmark01_<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591 (void);
// 0x00000074 System.Void TMPro.Examples.Benchmark01_<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B (void);
// 0x00000075 System.Object TMPro.Examples.Benchmark01_<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E (void);
// 0x00000076 System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4 (void);
// 0x00000077 System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376 (void);
// 0x00000078 System.Void TMPro.Examples.Benchmark01_UGUI_<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651 (void);
// 0x00000079 System.Void TMPro.Examples.Benchmark01_UGUI_<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561 (void);
// 0x0000007A System.Boolean TMPro.Examples.Benchmark01_UGUI_<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1 (void);
// 0x0000007B System.Object TMPro.Examples.Benchmark01_UGUI_<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD (void);
// 0x0000007C System.Void TMPro.Examples.Benchmark01_UGUI_<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA (void);
// 0x0000007D System.Object TMPro.Examples.Benchmark01_UGUI_<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2 (void);
// 0x0000007E System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046 (void);
// 0x0000007F System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5 (void);
// 0x00000080 System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77 (void);
// 0x00000081 System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516 (void);
// 0x00000082 System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE (void);
// 0x00000083 System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9 (void);
// 0x00000084 System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C (void);
// 0x00000085 System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E (void);
// 0x00000086 System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610 (void);
// 0x00000087 System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373 (void);
// 0x00000088 System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3 (void);
// 0x00000089 System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124 (void);
// 0x0000008A System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA (void);
// 0x0000008B System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3 (void);
// 0x0000008C System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC (void);
// 0x0000008D System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8 (void);
// 0x0000008E System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2 (void);
// 0x0000008F System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469 (void);
// 0x00000090 System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64 (void);
// 0x00000091 System.Void TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A (void);
// 0x00000092 System.Void TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B (void);
// 0x00000093 System.Boolean TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF (void);
// 0x00000094 System.Object TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390 (void);
// 0x00000095 System.Void TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B (void);
// 0x00000096 System.Object TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362 (void);
// 0x00000097 System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F (void);
// 0x00000098 System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5 (void);
// 0x00000099 System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697 (void);
// 0x0000009A System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB (void);
// 0x0000009B System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F (void);
// 0x0000009C UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3 (void);
// 0x0000009D System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3 (void);
// 0x0000009E System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86 (void);
// 0x0000009F System.Void TMPro.Examples.SkewTextExample_<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F (void);
// 0x000000A0 System.Void TMPro.Examples.SkewTextExample_<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF (void);
// 0x000000A1 System.Boolean TMPro.Examples.SkewTextExample_<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF (void);
// 0x000000A2 System.Object TMPro.Examples.SkewTextExample_<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1 (void);
// 0x000000A3 System.Void TMPro.Examples.SkewTextExample_<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F (void);
// 0x000000A4 System.Object TMPro.Examples.SkewTextExample_<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A (void);
// 0x000000A5 System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4 (void);
// 0x000000A6 System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB (void);
// 0x000000A7 System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12 (void);
// 0x000000A8 System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3 (void);
// 0x000000A9 System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720 (void);
// 0x000000AA System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21 (void);
// 0x000000AB System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter_FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1 (void);
// 0x000000AC System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C (void);
// 0x000000AD System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF (void);
// 0x000000AE System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A (void);
// 0x000000AF System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D (void);
// 0x000000B0 System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0 (void);
// 0x000000B1 System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B (void);
// 0x000000B2 System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363 (void);
// 0x000000B3 System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C (void);
// 0x000000B4 System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B (void);
// 0x000000B5 System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE (void);
// 0x000000B6 System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943 (void);
// 0x000000B7 System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD (void);
// 0x000000B8 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0 (void);
// 0x000000B9 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66 (void);
// 0x000000BA System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF (void);
// 0x000000BB System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A (void);
// 0x000000BC System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB (void);
// 0x000000BD System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781 (void);
// 0x000000BE System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910 (void);
// 0x000000BF System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691 (void);
// 0x000000C0 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D (void);
// 0x000000C1 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F (void);
// 0x000000C2 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F (void);
// 0x000000C3 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC (void);
// 0x000000C4 System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2 (void);
// 0x000000C5 System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33 (void);
// 0x000000C6 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E (void);
// 0x000000C7 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944 (void);
// 0x000000C8 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749 (void);
// 0x000000C9 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter_FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647 (void);
// 0x000000CA System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84 (void);
// 0x000000CB System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC (void);
// 0x000000CC System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay_FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB (void);
// 0x000000CD System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF (void);
// 0x000000CE System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE (void);
// 0x000000CF System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7 (void);
// 0x000000D0 System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF (void);
// 0x000000D1 System.Void TMPro.Examples.TeleType_<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629 (void);
// 0x000000D2 System.Void TMPro.Examples.TeleType_<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9 (void);
// 0x000000D3 System.Boolean TMPro.Examples.TeleType_<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715 (void);
// 0x000000D4 System.Object TMPro.Examples.TeleType_<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B (void);
// 0x000000D5 System.Void TMPro.Examples.TeleType_<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8 (void);
// 0x000000D6 System.Object TMPro.Examples.TeleType_<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261 (void);
// 0x000000D7 System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0 (void);
// 0x000000D8 System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA (void);
// 0x000000D9 System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50 (void);
// 0x000000DA System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712 (void);
// 0x000000DB System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8 (void);
// 0x000000DC System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534 (void);
// 0x000000DD System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4 (void);
// 0x000000DE System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848 (void);
// 0x000000DF System.Void TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D (void);
// 0x000000E0 System.Void TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C (void);
// 0x000000E1 System.Boolean TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3 (void);
// 0x000000E2 System.Object TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83 (void);
// 0x000000E3 System.Void TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691 (void);
// 0x000000E4 System.Object TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F (void);
// 0x000000E5 System.Void TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0 (void);
// 0x000000E6 System.Void TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8 (void);
// 0x000000E7 System.Boolean TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210 (void);
// 0x000000E8 System.Object TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB (void);
// 0x000000E9 System.Void TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F (void);
// 0x000000EA System.Object TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27 (void);
// 0x000000EB System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046 (void);
// 0x000000EC System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1 (void);
// 0x000000ED System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7 (void);
// 0x000000EE System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF (void);
// 0x000000EF System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E (void);
// 0x000000F0 System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12__ctor_mA27BBC06A409A9D9FB03E0D2C74677486B80D168 (void);
// 0x000000F1 System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_IDisposable_Dispose_m510FB73335FCBCBEC6AC61A4F2A0217722BF27FC (void);
// 0x000000F2 System.Boolean TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_MoveNext_m8DFB6850F5F9B8DF05173D4CB9C76B997EC87B57 (void);
// 0x000000F3 System.Object TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCD7B5218C81C0872AB501CD54626F1284A4F73BF (void);
// 0x000000F4 System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_Reset_mD2D32B94CA8D5A6D502BA38BDE1969C856368F73 (void);
// 0x000000F5 System.Object TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_get_Current_m59B2488B0E3C72CE97659E8D19BB13C6E0A44E90 (void);
// 0x000000F6 System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__13__ctor_m359EAC129649F98C759B341846A6DC07F95230D2 (void);
// 0x000000F7 System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_IDisposable_Dispose_m36DE8722B670A7DDD9E70107024590B20A4E0B18 (void);
// 0x000000F8 System.Boolean TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_MoveNext_mB7D320C272AAA835590B8460DA89DEBC0A243815 (void);
// 0x000000F9 System.Object TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m891591B80D28B1DF2CF9EB78C19DA672A81231A2 (void);
// 0x000000FA System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_Reset_m5EF21CA1E0C64E67D18D9355B5E064C93452F5B2 (void);
// 0x000000FB System.Object TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_get_Current_m4850A72D887C4DA3F53CA1A1A2BEAD5C5C6605A1 (void);
// 0x000000FC System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB (void);
// 0x000000FD System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA (void);
// 0x000000FE System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1 (void);
// 0x000000FF System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC (void);
// 0x00000100 System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66 (void);
// 0x00000101 System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42 (void);
// 0x00000102 System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9 (void);
// 0x00000103 System.Void TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F (void);
// 0x00000104 System.Void TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007 (void);
// 0x00000105 System.Boolean TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6 (void);
// 0x00000106 System.Object TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51 (void);
// 0x00000107 System.Void TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932 (void);
// 0x00000108 System.Object TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52 (void);
// 0x00000109 System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F (void);
// 0x0000010A System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825 (void);
// 0x0000010B System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846 (void);
// 0x0000010C System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97 (void);
// 0x0000010D System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549 (void);
// 0x0000010E System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16 (void);
// 0x0000010F System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE (void);
// 0x00000110 System.Void TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8 (void);
// 0x00000111 System.Void TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099 (void);
// 0x00000112 System.Boolean TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B (void);
// 0x00000113 System.Object TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479 (void);
// 0x00000114 System.Void TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D (void);
// 0x00000115 System.Object TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF (void);
// 0x00000116 System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2 (void);
// 0x00000117 System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA (void);
// 0x00000118 System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2 (void);
// 0x00000119 System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE (void);
// 0x0000011A System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E (void);
// 0x0000011B System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599 (void);
// 0x0000011C System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62 (void);
// 0x0000011D System.Void TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2 (void);
// 0x0000011E System.Void TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81 (void);
// 0x0000011F System.Boolean TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F (void);
// 0x00000120 System.Object TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE (void);
// 0x00000121 System.Void TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA (void);
// 0x00000122 System.Object TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E (void);
// 0x00000123 System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5 (void);
// 0x00000124 System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971 (void);
// 0x00000125 System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4 (void);
// 0x00000126 System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1 (void);
// 0x00000127 System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5 (void);
// 0x00000128 System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87 (void);
// 0x00000129 System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC (void);
// 0x0000012A System.Void TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760 (void);
// 0x0000012B System.Void TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC (void);
// 0x0000012C System.Boolean TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E (void);
// 0x0000012D System.Object TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D (void);
// 0x0000012E System.Void TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB (void);
// 0x0000012F System.Object TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD (void);
// 0x00000130 System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C (void);
// 0x00000131 System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC (void);
// 0x00000132 System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884 (void);
// 0x00000133 System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C (void);
// 0x00000134 System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F (void);
// 0x00000135 System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED (void);
// 0x00000136 System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2 (void);
// 0x00000137 System.Void TMPro.Examples.VertexZoom_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348 (void);
// 0x00000138 System.Int32 TMPro.Examples.VertexZoom_<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107 (void);
// 0x00000139 System.Void TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2 (void);
// 0x0000013A System.Void TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F (void);
// 0x0000013B System.Boolean TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66 (void);
// 0x0000013C System.Object TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9 (void);
// 0x0000013D System.Void TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C (void);
// 0x0000013E System.Object TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F (void);
// 0x0000013F System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0 (void);
// 0x00000140 System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4 (void);
// 0x00000141 UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB (void);
// 0x00000142 System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2 (void);
// 0x00000143 System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099 (void);
// 0x00000144 System.Void TMPro.Examples.WarpTextExample_<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49 (void);
// 0x00000145 System.Void TMPro.Examples.WarpTextExample_<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60 (void);
// 0x00000146 System.Boolean TMPro.Examples.WarpTextExample_<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84 (void);
// 0x00000147 System.Object TMPro.Examples.WarpTextExample_<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1 (void);
// 0x00000148 System.Void TMPro.Examples.WarpTextExample_<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535 (void);
// 0x00000149 System.Object TMPro.Examples.WarpTextExample_<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25 (void);
// 0x0000014A DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleAudio::DOFade(UnityEngine.AudioSource,System.Single,System.Single)
extern void DOTweenModuleAudio_DOFade_mEA99A941484D71522BCDD1CCC43B790A6AF4A0B3 (void);
// 0x0000014B DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleAudio::DOPitch(UnityEngine.AudioSource,System.Single,System.Single)
extern void DOTweenModuleAudio_DOPitch_m6512EC4BEE6C70E110DDFC1048BFCE13FA87E914 (void);
// 0x0000014C DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleAudio::DOSetFloat(UnityEngine.Audio.AudioMixer,System.String,System.Single,System.Single)
extern void DOTweenModuleAudio_DOSetFloat_m8831E89C2E951DDAC9C20A46D3AAD329BAD9C09C (void);
// 0x0000014D System.Int32 DG.Tweening.DOTweenModuleAudio::DOComplete(UnityEngine.Audio.AudioMixer,System.Boolean)
extern void DOTweenModuleAudio_DOComplete_m49C5BD0D315241E09EA8D6398F8011C5A34646AE (void);
// 0x0000014E System.Int32 DG.Tweening.DOTweenModuleAudio::DOKill(UnityEngine.Audio.AudioMixer,System.Boolean)
extern void DOTweenModuleAudio_DOKill_m2CED6088E868165A2D91768409FAFFD980993747 (void);
// 0x0000014F System.Int32 DG.Tweening.DOTweenModuleAudio::DOFlip(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOFlip_m080FDDE3ECDF68901289FBC6EA1297EF278D9D36 (void);
// 0x00000150 System.Int32 DG.Tweening.DOTweenModuleAudio::DOGoto(UnityEngine.Audio.AudioMixer,System.Single,System.Boolean)
extern void DOTweenModuleAudio_DOGoto_mD4A9A702C6187927FDBDD3220570820533459D16 (void);
// 0x00000151 System.Int32 DG.Tweening.DOTweenModuleAudio::DOPause(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPause_mD42B2DFA89EA1635A6F9E813F7B852C852F282E5 (void);
// 0x00000152 System.Int32 DG.Tweening.DOTweenModuleAudio::DOPlay(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPlay_m95942072A27A597B1FF4FA58A56EC35EC3A8DC15 (void);
// 0x00000153 System.Int32 DG.Tweening.DOTweenModuleAudio::DOPlayBackwards(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPlayBackwards_mA2814EE20AB3527ADC90DAC391BDF4AD2DA63104 (void);
// 0x00000154 System.Int32 DG.Tweening.DOTweenModuleAudio::DOPlayForward(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPlayForward_m808D504EA5FCA7B43292D4024735C69339BEC168 (void);
// 0x00000155 System.Int32 DG.Tweening.DOTweenModuleAudio::DORestart(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DORestart_m53CC4DB3E3B69233410704AC28B0E7E5FE15F9B6 (void);
// 0x00000156 System.Int32 DG.Tweening.DOTweenModuleAudio::DORewind(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DORewind_m1102048CF3E759CC5A0B7AF48B37092DD8625D5E (void);
// 0x00000157 System.Int32 DG.Tweening.DOTweenModuleAudio::DOSmoothRewind(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOSmoothRewind_m4C703A054EDA680CDA0FE09924AD97775BF70DA1 (void);
// 0x00000158 System.Int32 DG.Tweening.DOTweenModuleAudio::DOTogglePause(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOTogglePause_mA1AE6D3A3A8FA9692CE0695E915AA08626EF61CF (void);
// 0x00000159 System.Void DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_mB8ECD32BCDE8AEE5DA844CB16FB319546FDF65F4 (void);
// 0x0000015A System.Single DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass0_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m6447B544882FDCCECF5F03C92CFD462E52F55C3C (void);
// 0x0000015B System.Void DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass0_0::<DOFade>b__1(System.Single)
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_mCBA7DC56068D62779FD9318BF1BF6CC04674AA73 (void);
// 0x0000015C System.Void DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_mD593A9B61B5F03AF9D1695ED9FA2C1E993F41739 (void);
// 0x0000015D System.Single DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass1_0::<DOPitch>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__0_mB6B65C2ED3194137382A4DC81F72AB5C2C95FEB7 (void);
// 0x0000015E System.Void DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass1_0::<DOPitch>b__1(System.Single)
extern void U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__1_m9ED5ED10167C9FA210B584AAC22893154A683518 (void);
// 0x0000015F System.Void DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m3D650640D73868903AB5235513EB20B1EF57851E (void);
// 0x00000160 System.Single DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass2_0::<DOSetFloat>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__0_m1B04734B7D8B9A386220DCDED11506098EDF5039 (void);
// 0x00000161 System.Void DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass2_0::<DOSetFloat>b__1(System.Single)
extern void U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__1_mDE42A0D8DA684D1F5E21CA1797A119D6BF6252C3 (void);
// 0x00000162 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMove(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMove_mAD664CEB3F56A94F3481FA83FC799F0D7F8FF53F (void);
// 0x00000163 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMoveX(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMoveX_m4CD527117E19DE2009884655A131DF83B9EAD4A9 (void);
// 0x00000164 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMoveY(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMoveY_m126BCA31B85BF67D1B5E85549A95802B69777A2C (void);
// 0x00000165 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMoveZ(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMoveZ_mDC3444100D64651B456A118C92B6F77C99C32597 (void);
// 0x00000166 DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions> DG.Tweening.DOTweenModulePhysics::DORotate(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern void DOTweenModulePhysics_DORotate_m2BDE8501CE323175A7471D694249E660F5BF068C (void);
// 0x00000167 DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions> DG.Tweening.DOTweenModulePhysics::DOLookAt(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,DG.Tweening.AxisConstraint,System.Nullable`1<UnityEngine.Vector3>)
extern void DOTweenModulePhysics_DOLookAt_m706C3BF65EDBCCB2562FB115E61151945941C5FC (void);
// 0x00000168 DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics::DOJump(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOJump_mCDD95D56D93498D68A345E57B3C359388A816821 (void);
// 0x00000169 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOPath(UnityEngine.Rigidbody,UnityEngine.Vector3[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics_DOPath_mF69DF997EF91CD5347391BC89E6CE268A5CD39DD (void);
// 0x0000016A DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOLocalPath(UnityEngine.Rigidbody,UnityEngine.Vector3[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics_DOLocalPath_mB056B81341D5048E516138EBE4A680B3F4664D6D (void);
// 0x0000016B DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void DOTweenModulePhysics_DOPath_m3C49FFEB71D494F474F7223248D7105E90CF5BE7 (void);
// 0x0000016C DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOLocalPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void DOTweenModulePhysics_DOLocalPath_m6F939DAF66D56DA9B52DA62546C70F3862E14193 (void);
// 0x0000016D System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_mCFBBC383950DE52D74C6F4E85C4F7F3273B10719 (void);
// 0x0000016E UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass0_0::<DOMove>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_m8158605D880C075EEDC090F3BD1175AA33E2E107 (void);
// 0x0000016F System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m4E41921AB96267B39AAB22B0790884BEB58B467D (void);
// 0x00000170 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass1_0::<DOMoveX>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_m0EEE1A4D5C020E63505FB9F7E09A531433FC817D (void);
// 0x00000171 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mA7A465D617CE4C839F719F65F44B2D806976FC71 (void);
// 0x00000172 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass2_0::<DOMoveY>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_m4930252334FB6162D77F0821409D7487A96CB696 (void);
// 0x00000173 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m9E94ECE0838CF218AB657F8C4B51F881B310EF34 (void);
// 0x00000174 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass3_0::<DOMoveZ>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOMoveZU3Eb__0_mF75D2479FE18CCE0BFC07AA4A06B1EBE01369D29 (void);
// 0x00000175 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m1A01BE964AEBF9737E72E7C9B3B5D660D965B936 (void);
// 0x00000176 UnityEngine.Quaternion DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass4_0::<DORotate>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDORotateU3Eb__0_m6B41E1A9419EDB0B9FDE8C9A62BDAA1281D19EF5 (void);
// 0x00000177 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_mF63278B7093557C5BF31EE7878AC0E87D82EEC74 (void);
// 0x00000178 UnityEngine.Quaternion DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass5_0::<DOLookAt>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CDOLookAtU3Eb__0_m2019767096427977C234F4A6698E6C2559840795 (void);
// 0x00000179 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m06AC3FA2094299E1FDE7ECAFCC8F970C5139B4FE (void);
// 0x0000017A UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::<DOJump>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__0_m847B6B84B9DBBB179602A2DAC20A1E47B7CD0C6F (void);
// 0x0000017B System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::<DOJump>b__1()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__1_m488237F614033272B64730F763C4EFCD4009ACDB (void);
// 0x0000017C UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::<DOJump>b__2()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__2_m6F586BA8E7E757466B5E77AB61BD0056F4D86D28 (void);
// 0x0000017D UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::<DOJump>b__3()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__3_mEC9264AAF5B45E16B176AFA00079BFDA173EDDFE (void);
// 0x0000017E System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::<DOJump>b__4()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__4_m15CD7F1F9A30E34BA9AC943FA510B9FF0C15F84E (void);
// 0x0000017F System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m177CD3D0B0169C7DF1FD6F64FB8C39D0F92E7023 (void);
// 0x00000180 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass7_0::<DOPath>b__0()
extern void U3CU3Ec__DisplayClass7_0_U3CDOPathU3Eb__0_m448E92A163A6A98FFE561134611FAD6418C6824B (void);
// 0x00000181 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m4499D3AB924EEB9EB850233A8F64E5EC4A7AEA3D (void);
// 0x00000182 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass8_0::<DOLocalPath>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__0_mAB235341BB5079CA397BF09B075B856C6B950EDF (void);
// 0x00000183 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass8_0::<DOLocalPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__1_m19CF06E0D31E737EFDEEFE6D4D2361EA0D372730 (void);
// 0x00000184 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_mCA1E6F241784A13DEC15E12A0219479D552BB6BB (void);
// 0x00000185 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass9_0::<DOPath>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOPathU3Eb__0_mD050544C13BA54260FC94E0964EF204B4EDB2628 (void);
// 0x00000186 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m819824BC9854A0C90662B04D48B88CA7B052BF83 (void);
// 0x00000187 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass10_0::<DOLocalPath>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__0_m3089CA33F25546258A5E846EA51421D71AC55D70 (void);
// 0x00000188 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass10_0::<DOLocalPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__1_mD201EFBB4403FBDEC0D377D39A768A581E6CCDDD (void);
// 0x00000189 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics2D::DOMove(UnityEngine.Rigidbody2D,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOMove_mC7C9AD67230F89E53BCC36B37BC699E6E3DBFCC9 (void);
// 0x0000018A DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics2D::DOMoveX(UnityEngine.Rigidbody2D,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOMoveX_m47B42CE055F1892B75820F6FDE992E541394D23E (void);
// 0x0000018B DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics2D::DOMoveY(UnityEngine.Rigidbody2D,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOMoveY_mBFBABC3C73C6D858C280B8E0E21869BCFC57938F (void);
// 0x0000018C DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModulePhysics2D::DORotate(UnityEngine.Rigidbody2D,System.Single,System.Single)
extern void DOTweenModulePhysics2D_DORotate_m8E6DBA35EFB07C71A288D36A86B1299D99D2BE6A (void);
// 0x0000018D DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics2D::DOJump(UnityEngine.Rigidbody2D,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOJump_m13774D66ED37FC3FF7BA6862C306D32C0B01C351 (void);
// 0x0000018E DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics2D::DOPath(UnityEngine.Rigidbody2D,UnityEngine.Vector2[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics2D_DOPath_mC8E35580B3D00EC82A6EB5150838D2D220978092 (void);
// 0x0000018F DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics2D::DOLocalPath(UnityEngine.Rigidbody2D,UnityEngine.Vector2[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics2D_DOLocalPath_m56F836B516F2576C6CF48E07518EF403ED233844 (void);
// 0x00000190 System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m28BD574ECE1FAC2A8040F507B4347CA641B883C8 (void);
// 0x00000191 UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass0_0::<DOMove>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_mD0BB74EE7E3ED3D15C2B501656DD41CB4DB1CFB0 (void);
// 0x00000192 System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m0A358EE93B5CBA3BAAFED5EFA3CB29B852CC0A8C (void);
// 0x00000193 UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass1_0::<DOMoveX>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_m890A07E5B25CF040E903014587C8C30FC43505CF (void);
// 0x00000194 System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m79644C3E99A1BADC2CF695A33996EFBE999BCA28 (void);
// 0x00000195 UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass2_0::<DOMoveY>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_m833E99A0F3AFA6D89F501FE08B0EF7371AE20D4C (void);
// 0x00000196 System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m37C53B07CACF2CE16A6FECB187B45E328882E6CC (void);
// 0x00000197 System.Single DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass3_0::<DORotate>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDORotateU3Eb__0_mE8799B2B2CD516708D2235A0D1822AF9AEA117DF (void);
// 0x00000198 System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m35FA34381650BF0B9A9C29EE83B8043082D0A482 (void);
// 0x00000199 UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::<DOJump>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__0_m66684B2056E4B92E933AD98C2EAD6EDD9AF9C174 (void);
// 0x0000019A System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::<DOJump>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__1_m99B395C93C8827215768541DBD48AAFCAA4014EC (void);
// 0x0000019B System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::<DOJump>b__2()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__2_m9D2C7FBC51C4DBCCD91E8359A6FCD6D5BD633FB7 (void);
// 0x0000019C UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::<DOJump>b__3()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__3_mEC32920DB355DC7E1AAF2249C33F3177670806E8 (void);
// 0x0000019D System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::<DOJump>b__4(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__4_m053F71A269824B99AA058986F6E772F950C453C7 (void);
// 0x0000019E System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::<DOJump>b__5()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__5_mC21E068A8BCD02C771D362D4397E7C1113CC2E3D (void);
// 0x0000019F System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_mC0712263E288AF8337D78ACD1B4187EC2A79507D (void);
// 0x000001A0 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass5_0::<DOPath>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CDOPathU3Eb__0_m2DD131B5B01242CEE25D2865DF2F0BC59A4753B0 (void);
// 0x000001A1 System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass5_0::<DOPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass5_0_U3CDOPathU3Eb__1_m80D3EF48ACDCE8FCE54C12A18D6A47AEC649ECBC (void);
// 0x000001A2 System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m56FEA3991E5AE4A9842780C1DAFF6AF473C21946 (void);
// 0x000001A3 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass6_0::<DOLocalPath>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3CDOLocalPathU3Eb__0_mC6200A1DCC1B49838E2F65C06843DA00BBBA220C (void);
// 0x000001A4 System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass6_0::<DOLocalPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass6_0_U3CDOLocalPathU3Eb__1_mA1E8B523414CEE79D40909E7D32EE7347C0E7A8B (void);
// 0x000001A5 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleSprite::DOColor(UnityEngine.SpriteRenderer,UnityEngine.Color,System.Single)
extern void DOTweenModuleSprite_DOColor_mA67D7B77174B479F8AE20F739A74BA7A2BBC9F0F (void);
// 0x000001A6 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleSprite::DOFade(UnityEngine.SpriteRenderer,System.Single,System.Single)
extern void DOTweenModuleSprite_DOFade_mEED04CEE3EE9D32B87478A3CFDB9CFB710307769 (void);
// 0x000001A7 DG.Tweening.Sequence DG.Tweening.DOTweenModuleSprite::DOGradientColor(UnityEngine.SpriteRenderer,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleSprite_DOGradientColor_m47670ABD64AEDC972E8210CAF4C8A778B0B7676D (void);
// 0x000001A8 DG.Tweening.Tweener DG.Tweening.DOTweenModuleSprite::DOBlendableColor(UnityEngine.SpriteRenderer,UnityEngine.Color,System.Single)
extern void DOTweenModuleSprite_DOBlendableColor_m51BBDB4A69B0560481736E6734EA2BF52DB1537D (void);
// 0x000001A9 System.Void DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_mD77E24E24423ADEC43F9983504BD6796D2671E99 (void);
// 0x000001AA UnityEngine.Color DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass0_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__0_m12BAC3674340BDF408F41A0E9BEEF6BB87C3F504 (void);
// 0x000001AB System.Void DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass0_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__1_m89E34AE3BBC636BD2B38C1AB7300F1F20F25E616 (void);
// 0x000001AC System.Void DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m40F18C57ED175BA68D1D65FFA97879AEAF2E3325 (void);
// 0x000001AD UnityEngine.Color DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass1_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__0_m8B9E647B54E5DA35EE71D65866372C8C90B7F452 (void);
// 0x000001AE System.Void DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass1_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__1_m935CE2E2B7CB25D7151DA0BF3B4420C8099E0A45 (void);
// 0x000001AF System.Void DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mB0AA92A12DE97E8954819AC2BAD916065D4BECDB (void);
// 0x000001B0 UnityEngine.Color DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass3_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__0_m371A7E7E53DB5FC70A6534A7956B27A3A8B7EA03 (void);
// 0x000001B1 System.Void DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass3_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__1_m36515014C810F349D12C86DC636349E452DA6974 (void);
// 0x000001B2 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.CanvasGroup,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_mF927A1EDCA91814D8A466A343E3223EEF4ED7478 (void);
// 0x000001B3 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Graphic,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_m7067683474EE6A600294C2CCFD941403FA789A6E (void);
// 0x000001B4 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Graphic,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m41638D05EB7678C523D0D39E320FAC80B8E48658 (void);
// 0x000001B5 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_mF4B41443760536F3C399612AD0283A52937D95CC (void);
// 0x000001B6 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Image,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_mE66C8B7D137E0DDE54AAA96F551811FB8BDB8754 (void);
// 0x000001B7 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOFillAmount(UnityEngine.UI.Image,System.Single,System.Single)
extern void DOTweenModuleUI_DOFillAmount_mE4ED52B826A6F8035320E980F7C701B4047C4DBA (void);
// 0x000001B8 DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI::DOGradientColor(UnityEngine.UI.Image,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleUI_DOGradientColor_mB4B8D1DEDA92464670EB8A6D985973477D51A4D5 (void);
// 0x000001B9 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOFlexibleSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOFlexibleSize_mF0916C4E7A906202EFBAEA89CC2B55A9C3CF48B3 (void);
// 0x000001BA DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOMinSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOMinSize_m54A6919625499AFD6F801C6EC9672965DBDC96E1 (void);
// 0x000001BB DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPreferredSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOPreferredSize_mE26AFB2C606F095483EDED449FB8A5A80C08A8DE (void);
// 0x000001BC DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Outline,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_m15BC0FCF035E3DEEC3CFC30465EDE0FE6FD6D2EA (void);
// 0x000001BD DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Outline,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m86DB80308D92FC4DFD3E981F9E958FF2A207EA74 (void);
// 0x000001BE DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOScale(UnityEngine.UI.Outline,UnityEngine.Vector2,System.Single)
extern void DOTweenModuleUI_DOScale_mFA27F29E3DFB6B1CFCAC3CE73F06DF3737A3EB06 (void);
// 0x000001BF DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos_m63999D434CE7E8F6BE66BBA92532DE9D98DC9F79 (void);
// 0x000001C0 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPosX(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPosX_m9B57E02E9902C7A3F1A8DD1021AF630C10478380 (void);
// 0x000001C1 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPosY(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPosY_m14CCA63E545D7C28AD1A76488D0084F55EE94CA8 (void);
// 0x000001C2 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3D(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3D_m6DEB7C4AE4CD2118BF83D268945340F3373DC7BB (void);
// 0x000001C3 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DX(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DX_mA40303AC769280BDE74EE58BE5AD80BEA427CE08 (void);
// 0x000001C4 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DY(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DY_mE6BE56AE421B959F22CBA6318BABC3CD367A7277 (void);
// 0x000001C5 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DZ(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DZ_m702F7D4FE9DC8E6116FB5E0DD478D2BB88ED1242 (void);
// 0x000001C6 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorMax(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorMax_mDD97F6A8B4468622E3808507CB6B89A8A109BDCE (void);
// 0x000001C7 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorMin(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorMin_m60C4775B1EEDBCF2CD3E96A8171C8A83FD7F400D (void);
// 0x000001C8 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivot(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single)
extern void DOTweenModuleUI_DOPivot_mF623939EDCC152F664FECC8B71E321B0AA13F0A9 (void);
// 0x000001C9 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivotX(UnityEngine.RectTransform,System.Single,System.Single)
extern void DOTweenModuleUI_DOPivotX_mC21C031A61BDB7C3A8087719F23CBC6D1D762D6B (void);
// 0x000001CA DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivotY(UnityEngine.RectTransform,System.Single,System.Single)
extern void DOTweenModuleUI_DOPivotY_m9922C28ADECA433362D3C6C816318CA00705996A (void);
// 0x000001CB DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOSizeDelta(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOSizeDelta_mC4FD07525E1CA5BBF577030D571CE0CCEFD4E6B7 (void);
// 0x000001CC DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOPunchAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOPunchAnchorPos_m058E3DA140B9C497727F05AEAA296425E585B9EA (void);
// 0x000001CD DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,System.Single,System.Int32,System.Single,System.Boolean,System.Boolean)
extern void DOTweenModuleUI_DOShakeAnchorPos_m89D1C74E940B9434B2234FB49C031AD8FD1170AB (void);
// 0x000001CE DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,UnityEngine.Vector2,System.Int32,System.Single,System.Boolean,System.Boolean)
extern void DOTweenModuleUI_DOShakeAnchorPos_m7818867062313A535AB2CD6F86267E2AB9357AC6 (void);
// 0x000001CF DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI::DOJumpAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOJumpAnchorPos_m7046EE1C82AF0159CFAAA460E4C3D22797625480 (void);
// 0x000001D0 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DONormalizedPos(UnityEngine.UI.ScrollRect,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DONormalizedPos_m300F112FE2F67D4DC718083340C7FD27BC96F248 (void);
// 0x000001D1 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOHorizontalNormalizedPos(UnityEngine.UI.ScrollRect,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOHorizontalNormalizedPos_mCB4BCD85B49C458BE871F890A86FEFE0D7F41B42 (void);
// 0x000001D2 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOVerticalNormalizedPos(UnityEngine.UI.ScrollRect,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOVerticalNormalizedPos_mC0269B4644816A0D5C09717CA92E4229CB31CDF1 (void);
// 0x000001D3 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOValue(UnityEngine.UI.Slider,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOValue_mFDF45D60F52C82AACD8B50859A51B8F315A44F08 (void);
// 0x000001D4 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_mFD36CA1EA683A0D2312EE87BCF3B0A924504E797 (void);
// 0x000001D5 DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions> DG.Tweening.DOTweenModuleUI::DOCounter(UnityEngine.UI.Text,System.Int32,System.Int32,System.Single,System.Boolean,System.Globalization.CultureInfo)
extern void DOTweenModuleUI_DOCounter_m6C87AB89E5B14F1FB82630CFF15791678D7DF055 (void);
// 0x000001D6 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Text,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m21F7CCBB732ABBD63BFD893D1A8D9837532A70D1 (void);
// 0x000001D7 DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions> DG.Tweening.DOTweenModuleUI::DOText(UnityEngine.UI.Text,System.String,System.Single,System.Boolean,DG.Tweening.ScrambleMode,System.String)
extern void DOTweenModuleUI_DOText_mC0932E45473278B2FFB1698954CC0EB0422C626F (void);
// 0x000001D8 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Graphic,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_m4C5E9A68BFBEE876E1FF8D8E9A4BA77856F144C1 (void);
// 0x000001D9 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_m18196516A24780A550314E9327868D6D6F3EAA4E (void);
// 0x000001DA DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_mC30D1F805FE976FB25EC361AB0ABA94413361B6F (void);
// 0x000001DB UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_Utils::SwitchToRectTransform(UnityEngine.RectTransform,UnityEngine.RectTransform)
extern void Utils_SwitchToRectTransform_m260A15449F1C7A8F4356730DF4A59A386C45D200 (void);
// 0x000001DC System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m14E6397A137D3CB8A7638F9254B4694553B4DC7C (void);
// 0x000001DD System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass0_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m6F0CD242FAEF332D726AD4CE2EB2691CCB3748B1 (void);
// 0x000001DE System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass0_0::<DOFade>b__1(System.Single)
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_mDBDE14547A6DF70C0ADB82403282C26FBB4F6A27 (void);
// 0x000001DF System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_mCB63EFBBE399EEB9892054E0FAB2AB42E2BBB886 (void);
// 0x000001E0 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass1_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__0_mE8D0790E2303D84F8D513854D9F004E5E3127A3C (void);
// 0x000001E1 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass1_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__1_m07CB6A64D93955BC07E8AE290B01F9F39195A5A9 (void);
// 0x000001E2 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m61C588C0764BA908B02EBAB1C19076EBB2898A9E (void);
// 0x000001E3 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass2_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__0_m85CC4106263DD670C5BF648B8A64C6BA84297B65 (void);
// 0x000001E4 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass2_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__1_m140BDA0B825925A5051A2565C055CCBBF005A680 (void);
// 0x000001E5 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m09228DABE56F6866E09591FE27C255A39A71E48D (void);
// 0x000001E6 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass3_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_mA7BF7CEB8AA94694104E8D048F5045D122A5D980 (void);
// 0x000001E7 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass3_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_mBC7108789E76320E9507A9087273C7D89227F0F3 (void);
// 0x000001E8 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m5CD451B862F3CC71D8D25BC3A69725FCDD6144C4 (void);
// 0x000001E9 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass4_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m507CD55516001BB4A76D7E58CB159B5394D6413C (void);
// 0x000001EA System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass4_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_mE65F1E4D380EC305AF0AE93E0315B0B23CA36310 (void);
// 0x000001EB System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m879B3843E0C43157D8044AD52E2F865EE50FD041 (void);
// 0x000001EC System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass5_0::<DOFillAmount>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__0_mAFE3629BC44AE61C84399809F742CEF0235004E1 (void);
// 0x000001ED System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass5_0::<DOFillAmount>b__1(System.Single)
extern void U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__1_m9EB6C21BCE8D1A08FF84815C05A6664637C510CE (void);
// 0x000001EE System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m47B2E10FFFD9FCACC88586DEF6A4E675DC9EC406 (void);
// 0x000001EF UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass7_0::<DOFlexibleSize>b__0()
extern void U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__0_m7A7EA5F66922413743B8C1AB4234645A32098EA8 (void);
// 0x000001F0 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass7_0::<DOFlexibleSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__1_m0E76B7C6F893FD2F7F24E302CB55882765DD1153 (void);
// 0x000001F1 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m0953E12771619B6F14FC14CA853AF5CA110A0553 (void);
// 0x000001F2 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass8_0::<DOMinSize>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__0_m9BD9F63B884498F9AA02C307EB1CA15FA29BD094 (void);
// 0x000001F3 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass8_0::<DOMinSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__1_m546E530F9DE46CA1359450860E62DCC3BD3D72B3 (void);
// 0x000001F4 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m3EB28823EF4D152A06983986C8120490212A8DF7 (void);
// 0x000001F5 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass9_0::<DOPreferredSize>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__0_mB87EAB728455B68D07773FA10DCA169804482CBD (void);
// 0x000001F6 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass9_0::<DOPreferredSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__1_m24617F67F48227A961C0543BF7925F8D2F2A90B6 (void);
// 0x000001F7 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m11212BA4C7EB97E0EE079E78FA1C95C02D517C75 (void);
// 0x000001F8 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass10_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__0_m08737F267CCB60C0CA0244070DA3C50845FC8B1F (void);
// 0x000001F9 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass10_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__1_mAFFD9D2D2F3FD2BE4E2423286098B9BCC1C0C193 (void);
// 0x000001FA System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m9B3BB08FF1DC2F3101E0D580891268445B7763CA (void);
// 0x000001FB UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass11_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__0_mA57545026A9AE0CB3A20B6FFCDCF6F2F1CDA6AB0 (void);
// 0x000001FC System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass11_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__1_mCC70305FC1C1B93A838838519D023AC608D3E23E (void);
// 0x000001FD System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m05F6F905C11A344C5681CE1CD406DE85940356E5 (void);
// 0x000001FE UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass12_0::<DOScale>b__0()
extern void U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__0_mEE927D8596DDB335403416BF63FF4E7E27D49D51 (void);
// 0x000001FF System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass12_0::<DOScale>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__1_m65C60213296203590F596B6A3481E4B8621F96D5 (void);
// 0x00000200 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_mC6D360265628BCFBFDD42CCAF1136983FDD186BE (void);
// 0x00000201 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass13_0::<DOAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__0_m693A41C7FCF87BDE6211DE0ADED05AA5C476D71F (void);
// 0x00000202 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass13_0::<DOAnchorPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__1_m0F8F9CA39BC8894705C1EE9983E59D24A7BF218B (void);
// 0x00000203 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_m3BB3CA37E5912C211B954D7EE850C77F0B04BFF6 (void);
// 0x00000204 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass14_0::<DOAnchorPosX>b__0()
extern void U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__0_m02BA203D0A6C9D64BC8149C197099DA29370C872 (void);
// 0x00000205 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass14_0::<DOAnchorPosX>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__1_m47B6E3E511B7E234B49FDBD4D6BC32E5EC92B1E9 (void);
// 0x00000206 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m011D15F0644147EABAFA4ED415CD3F5E8782CCE8 (void);
// 0x00000207 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass15_0::<DOAnchorPosY>b__0()
extern void U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__0_mCBF0F2691FBC7AED64284B0FB4CCD75792AAB51C (void);
// 0x00000208 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass15_0::<DOAnchorPosY>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__1_m0E6D417FD9062B34D8C0E8B58C4C688B39CD9603 (void);
// 0x00000209 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_mF4FDD3E32D1C9FDD48079A148410AAB0CF4855B2 (void);
// 0x0000020A UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass16_0::<DOAnchorPos3D>b__0()
extern void U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_mFBE3FE1AC3F56C148108234A9A1285F87EEF50E8 (void);
// 0x0000020B System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass16_0::<DOAnchorPos3D>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_mFEF34D901D1E18E74D7E93297BDD75ABF146514C (void);
// 0x0000020C System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_mEEC98B74C2A5EDE984A5A59F4289ADFADAB3F804 (void);
// 0x0000020D UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass17_0::<DOAnchorPos3DX>b__0()
extern void U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__0_mC8D86325D798D952B390643A8BAE1D995368D36A (void);
// 0x0000020E System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass17_0::<DOAnchorPos3DX>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__1_mAB78FA2C6EEC74D6E259B2ABF94ECBD3CB62DB82 (void);
// 0x0000020F System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_mE272C380064D4945F3C7FDC2357A8B765BC52841 (void);
// 0x00000210 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass18_0::<DOAnchorPos3DY>b__0()
extern void U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__0_m8C53152ED72D09B8F2BE2DE14963561650D0C42B (void);
// 0x00000211 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass18_0::<DOAnchorPos3DY>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__1_m9E1FC98A6DF461320AC4B6F294F1A69AFD058E3E (void);
// 0x00000212 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass19_0::.ctor()
extern void U3CU3Ec__DisplayClass19_0__ctor_mF4A0F29D8F3315E7DD0BC8103DA0023E16C341CB (void);
// 0x00000213 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass19_0::<DOAnchorPos3DZ>b__0()
extern void U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__0_mD27E4DF91E21C64D13997EA12A7659B1E31AA77B (void);
// 0x00000214 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass19_0::<DOAnchorPos3DZ>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__1_mD6F74CB34BAEA91A512586538770AB27FFB68A1D (void);
// 0x00000215 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_mCE70C1CB9A605F65BE4D31D224111EE4C6FB7DE6 (void);
// 0x00000216 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass20_0::<DOAnchorMax>b__0()
extern void U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__0_mAB5724DF7472A7CED2070B75A1B0E12378D5E6DD (void);
// 0x00000217 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass20_0::<DOAnchorMax>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__1_m7E357679D17273D809A39CDF435B5E64E46A64A0 (void);
// 0x00000218 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_m47D7A3B4DA7AE2FE2F83164406834CF9150E5308 (void);
// 0x00000219 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass21_0::<DOAnchorMin>b__0()
extern void U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__0_m4804C6D34689DD21311DA86CC22008D3B0774391 (void);
// 0x0000021A System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass21_0::<DOAnchorMin>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__1_m73EF1691143A0E7E0254A73F52FDC73BB5AAA3E6 (void);
// 0x0000021B System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_m0D4210D174301CE26FA7A519B4168E7D87F6D92A (void);
// 0x0000021C UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass22_0::<DOPivot>b__0()
extern void U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__0_mB3D6F3AADE069EED625038A53491AFEA1DAFD9DF (void);
// 0x0000021D System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass22_0::<DOPivot>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__1_mDCA8630A9398A411ACB0950973AB99C0723CC074 (void);
// 0x0000021E System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_mA47954A6646E43342880718EE9B5E66A0D18FDF2 (void);
// 0x0000021F UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass23_0::<DOPivotX>b__0()
extern void U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__0_mFC16CCA332D908526AC3DC2BD8B4456969000CF5 (void);
// 0x00000220 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass23_0::<DOPivotX>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__1_m7E0DAFAB836AC4F133B995B50578D8911BA3113B (void);
// 0x00000221 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_m0524772292383368F9C9F6BDFF0A114D60A35F8F (void);
// 0x00000222 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass24_0::<DOPivotY>b__0()
extern void U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__0_m959088FDF2C1CD6EF40EE35BA531719E60C5327D (void);
// 0x00000223 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass24_0::<DOPivotY>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__1_mCECF34E3A27F03AA2DB5719CA1241927DC99BC5B (void);
// 0x00000224 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_m1ABD5A4A91526CA156C55C23A54822DC898A34AF (void);
// 0x00000225 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass25_0::<DOSizeDelta>b__0()
extern void U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__0_mF87E563BB15F601A91B0DA1012A1604A52FCBDA5 (void);
// 0x00000226 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass25_0::<DOSizeDelta>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__1_m26B3B99EDB1D47D81931CC6CABFED8406BF4E90E (void);
// 0x00000227 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_mC4836A2FD37334971703D95C49ED35BCAD56AFB8 (void);
// 0x00000228 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass26_0::<DOPunchAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__0_m357B263CA209B3E6EBB7F95AC352AA8B28A5CFC5 (void);
// 0x00000229 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass26_0::<DOPunchAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__1_mE7127F5C6AB73DBA7D40F67FD61BA4076E2B8D23 (void);
// 0x0000022A System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass27_0::.ctor()
extern void U3CU3Ec__DisplayClass27_0__ctor_m07ADC405B65035A17DC97097DED9B4C1D21EFB17 (void);
// 0x0000022B UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass27_0::<DOShakeAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__0_m1AF2B166591EEA50BEE7FD3C2E9B7EFFC0D39F8E (void);
// 0x0000022C System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass27_0::<DOShakeAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__1_mF7180DEAAB00C14DD6F3DB8B815D0C0B2DB64725 (void);
// 0x0000022D System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass28_0::.ctor()
extern void U3CU3Ec__DisplayClass28_0__ctor_m4969157094B09547B26225881BD87025C928B506 (void);
// 0x0000022E UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass28_0::<DOShakeAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__0_mB4E05CBCDBD4732D154E1A64678339D3FC5FBCE9 (void);
// 0x0000022F System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass28_0::<DOShakeAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__1_mBDA6378719836E14C155B4648BB47429C125590C (void);
// 0x00000230 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_m00E6EAFA57923B235554FA8A236AD64992F363FE (void);
// 0x00000231 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__0_mAE0CBA3D52D03EB2F2C37D85C5832B412F816591 (void);
// 0x00000232 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__1_mB685B77262E778382FAF923C04FD328DC494426C (void);
// 0x00000233 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__2()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__2_mB23D92AF70DCD33F838372FF59B1F39DD3775824 (void);
// 0x00000234 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__3()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__3_mB86E86358B9C1671706EA489B6FC30C3874224E7 (void);
// 0x00000235 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__4(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__4_m1E0D9286E08822DF086E86BA60F0EFF6B62A32C7 (void);
// 0x00000236 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__5()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__5_m6E6A274B0B852663D3ED8CDD2B4646B9D618E535 (void);
// 0x00000237 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_mCC70550D8AD7B4F88C913940A644E81F11F5898A (void);
// 0x00000238 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass30_0::<DONormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__0_m92C7B1817960CE4D1D7333872D37C8AF131F68FE (void);
// 0x00000239 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass30_0::<DONormalizedPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__1_mFA97248E3617865FEF6C3F36E633266FF044050F (void);
// 0x0000023A System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass31_0::.ctor()
extern void U3CU3Ec__DisplayClass31_0__ctor_m82279F879253DF896CCD126EC0B7E16B295D165A (void);
// 0x0000023B System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass31_0::<DOHorizontalNormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__0_mB33367FCE1348BA9F855A9D1366FA19F5DDA4AEB (void);
// 0x0000023C System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass31_0::<DOHorizontalNormalizedPos>b__1(System.Single)
extern void U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__1_mEBA7CDAA06332B1E56991D8E2299E24B9348D627 (void);
// 0x0000023D System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass32_0::.ctor()
extern void U3CU3Ec__DisplayClass32_0__ctor_mD93F08EDF6689C6BA223067191EE8C9E0FBE509C (void);
// 0x0000023E System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass32_0::<DOVerticalNormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__0_m83DD02084574CCFB9F26C19BDE6AF6DE2C6387FE (void);
// 0x0000023F System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass32_0::<DOVerticalNormalizedPos>b__1(System.Single)
extern void U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__1_mAE94A6E7E27147AA530D056E286A00C8EAE56331 (void);
// 0x00000240 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_mB387BC26BCA1B460D5B3B1CE767228FA244B7703 (void);
// 0x00000241 System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass33_0::<DOValue>b__0()
extern void U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__0_mBB603DBBDA542D7F00142102A667BCB9682EC145 (void);
// 0x00000242 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass33_0::<DOValue>b__1(System.Single)
extern void U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__1_mA3DF76574D5E76992AF228A3C4AFFFC19D6BAE15 (void);
// 0x00000243 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_mA88FD675FEF2971D3F8145822B844ECFFF59BF17 (void);
// 0x00000244 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass34_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__0_m5DC2ED4C56B0A1D8CFCB8A4E2D2078EA79E8B66F (void);
// 0x00000245 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass34_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__1_m9BA1BADA179249B9F0AD1142BE826798FA114E13 (void);
// 0x00000246 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass35_0::.ctor()
extern void U3CU3Ec__DisplayClass35_0__ctor_m5533AE6A72AB74A97282032790D0DE81F0EAF811 (void);
// 0x00000247 System.Int32 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass35_0::<DOCounter>b__0()
extern void U3CU3Ec__DisplayClass35_0_U3CDOCounterU3Eb__0_m4E790A70F3934B1A2F06D5692F8550A69C2567A2 (void);
// 0x00000248 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass35_0::<DOCounter>b__1(System.Int32)
extern void U3CU3Ec__DisplayClass35_0_U3CDOCounterU3Eb__1_m3207CE6E96DAC0D117EFA238D72BAC0FAEFB2FE2 (void);
// 0x00000249 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_mE2912C6BF4318E28870DE7AA92E4B05C80D92EE0 (void);
// 0x0000024A UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass36_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__0_mA3238766629BBF7E2750683F62F7629C5B097D87 (void);
// 0x0000024B System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass36_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__1_m119575F5E470CA6D0C37EF9E1A9C3A2CF8B2AA3A (void);
// 0x0000024C System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass37_0::.ctor()
extern void U3CU3Ec__DisplayClass37_0__ctor_m59CE3FBA46AF3F7C3597AD84DEC898DB9B84EE39 (void);
// 0x0000024D System.String DG.Tweening.DOTweenModuleUI_<>c__DisplayClass37_0::<DOText>b__0()
extern void U3CU3Ec__DisplayClass37_0_U3CDOTextU3Eb__0_mDB021AB28041C55E82DF362F8DDF94879B7EBA13 (void);
// 0x0000024E System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass37_0::<DOText>b__1(System.String)
extern void U3CU3Ec__DisplayClass37_0_U3CDOTextU3Eb__1_m0724BC3E19FD8643DD8C0FF25D4C08196FB446DD (void);
// 0x0000024F System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass38_0::.ctor()
extern void U3CU3Ec__DisplayClass38_0__ctor_mB3E91AEAFAE65DD5FC36DD530E74CE1F4FA24AEF (void);
// 0x00000250 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass38_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__0_mB976655AEC70D25B43AAAF577CC953C7EB03D2EE (void);
// 0x00000251 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass38_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__1_mF14436C4F4EEB5B7664C3444B969B694BC6E3E5E (void);
// 0x00000252 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass39_0::.ctor()
extern void U3CU3Ec__DisplayClass39_0__ctor_m1DB7B55A6B3E4B184E35968EACBF9E214E0CED7D (void);
// 0x00000253 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass39_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__0_mE93AC95325F69D83EE746421FA5992DC25009961 (void);
// 0x00000254 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass39_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__1_m45DD6E7ED579816AC18DF6B88901D9F11DB1B36F (void);
// 0x00000255 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass40_0::.ctor()
extern void U3CU3Ec__DisplayClass40_0__ctor_m089AA2605E325970B89E7F1519CF4DD5676CA2AD (void);
// 0x00000256 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass40_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass40_0_U3CDOBlendableColorU3Eb__0_mD869D1DD9D45824831045C0E5638161837B0854B (void);
// 0x00000257 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass40_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass40_0_U3CDOBlendableColorU3Eb__1_m6B0A0EFCA7952B551FD8C87503133EA08D8075A9 (void);
// 0x00000258 DG.Tweening.Sequence DG.Tweening.DOTweenModuleUnityVersion::DOGradientColor(UnityEngine.Material,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleUnityVersion_DOGradientColor_m7E9B0D004D4B3D602EBB852CA1755FE863C6B839 (void);
// 0x00000259 DG.Tweening.Sequence DG.Tweening.DOTweenModuleUnityVersion::DOGradientColor(UnityEngine.Material,UnityEngine.Gradient,System.String,System.Single)
extern void DOTweenModuleUnityVersion_DOGradientColor_mA4E191E86F2EF81966259A40B2CDADE375437920 (void);
// 0x0000025A UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForCompletion(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForCompletion_m4BCEDFA5ED1ACF4BC1B3531C0C306A7AC553C1F2 (void);
// 0x0000025B UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForRewind(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForRewind_mD9F37D5AAD9B665EE07F083D026E65194A8E3E6E (void);
// 0x0000025C UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForKill(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForKill_m02BD7F3B018BB6B4ED4FCAF47B8DA435EC60BD6B (void);
// 0x0000025D UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForElapsedLoops(DG.Tweening.Tween,System.Int32,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForElapsedLoops_m46D46788899029A20F73BE252F7427D853131404 (void);
// 0x0000025E UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForPosition(DG.Tweening.Tween,System.Single,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForPosition_m1771625E294B38C5D13DCA40846AEA881C1B23E6 (void);
// 0x0000025F UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForStart(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForStart_mB4AAFE83B50AEF3A167F31C14B2F58794527F905 (void);
// 0x00000260 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUnityVersion::DOOffset(UnityEngine.Material,UnityEngine.Vector2,System.Int32,System.Single)
extern void DOTweenModuleUnityVersion_DOOffset_m09CA228EBED193314D3D9A41BBDC97393E1FC4D2 (void);
// 0x00000261 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUnityVersion::DOTiling(UnityEngine.Material,UnityEngine.Vector2,System.Int32,System.Single)
extern void DOTweenModuleUnityVersion_DOTiling_m6A3CD06EC3030A93EC3E93DE678F34048D5C1141 (void);
// 0x00000262 System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForCompletion(DG.Tweening.Tween)
extern void DOTweenModuleUnityVersion_AsyncWaitForCompletion_m886CBFB2DCD9455C2762F639F065A7AC9F1EC2E5 (void);
// 0x00000263 System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForRewind(DG.Tweening.Tween)
extern void DOTweenModuleUnityVersion_AsyncWaitForRewind_mCC0B2A591BA46DE9E5CB59EA84461051477D5642 (void);
// 0x00000264 System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForKill(DG.Tweening.Tween)
extern void DOTweenModuleUnityVersion_AsyncWaitForKill_m11E8731B8A8E89493FB7976A35DCF4DD85E2132B (void);
// 0x00000265 System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForElapsedLoops(DG.Tweening.Tween,System.Int32)
extern void DOTweenModuleUnityVersion_AsyncWaitForElapsedLoops_m9F3D38817E779AA9C5F0BA7811E6626A7BFBB5B1 (void);
// 0x00000266 System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForPosition(DG.Tweening.Tween,System.Single)
extern void DOTweenModuleUnityVersion_AsyncWaitForPosition_m6207EF50EF7DE8402EDC806F8FEF6CC313C4DB03 (void);
// 0x00000267 System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForStart(DG.Tweening.Tween)
extern void DOTweenModuleUnityVersion_AsyncWaitForStart_m637C3E8C05D413E080897E3A99CF21C4BD859F25 (void);
// 0x00000268 System.Void DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m0EACBC706CDB7B8BCE4C21D3AD250FE25CD825CF (void);
// 0x00000269 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass8_0::<DOOffset>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__0_m6D616025F8610086B30B83BC8ED13213DB2AC7CC (void);
// 0x0000026A System.Void DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass8_0::<DOOffset>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__1_m4171C9EB0226E0E06E574EFD507A8D352EC5B557 (void);
// 0x0000026B System.Void DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m6CC810F5A3B1779C2569B21779361FD5F94C2C9C (void);
// 0x0000026C UnityEngine.Vector2 DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass9_0::<DOTiling>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__0_m747CC91BE7EDF7D2644045B72BF689A2552B855F (void);
// 0x0000026D System.Void DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass9_0::<DOTiling>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__1_m0D5FC9115C0FFE113CA2277BA5A17562550B19F6 (void);
// 0x0000026E System.Void DG.Tweening.DOTweenModuleUnityVersion_<AsyncWaitForCompletion>d__10::MoveNext()
extern void U3CAsyncWaitForCompletionU3Ed__10_MoveNext_m7A14EB02262CB797C27664F35565B88E8BA060D3_AdjustorThunk (void);
// 0x0000026F System.Void DG.Tweening.DOTweenModuleUnityVersion_<AsyncWaitForCompletion>d__10::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForCompletionU3Ed__10_SetStateMachine_m85F5E4DAF7F7E7AB65762F3F6C95BFAE3F039819_AdjustorThunk (void);
// 0x00000270 System.Void DG.Tweening.DOTweenModuleUnityVersion_<AsyncWaitForRewind>d__11::MoveNext()
extern void U3CAsyncWaitForRewindU3Ed__11_MoveNext_m5DBC14BB1136B7BCF18066FB833D90CF116C1549_AdjustorThunk (void);
// 0x00000271 System.Void DG.Tweening.DOTweenModuleUnityVersion_<AsyncWaitForRewind>d__11::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForRewindU3Ed__11_SetStateMachine_mAC2B29B1F4653A143591E2BB2388F8AD9F9A0D64_AdjustorThunk (void);
// 0x00000272 System.Void DG.Tweening.DOTweenModuleUnityVersion_<AsyncWaitForKill>d__12::MoveNext()
extern void U3CAsyncWaitForKillU3Ed__12_MoveNext_mAA9DB56F9B8B3A6E9069CC690E90634D7F51A276_AdjustorThunk (void);
// 0x00000273 System.Void DG.Tweening.DOTweenModuleUnityVersion_<AsyncWaitForKill>d__12::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForKillU3Ed__12_SetStateMachine_m47EB31E97C542A8D7AC82CC7EAF613DC8EEDEABD_AdjustorThunk (void);
// 0x00000274 System.Void DG.Tweening.DOTweenModuleUnityVersion_<AsyncWaitForElapsedLoops>d__13::MoveNext()
extern void U3CAsyncWaitForElapsedLoopsU3Ed__13_MoveNext_mD74D8D5D0970E23A40CE0D8B83297BBC68E25712_AdjustorThunk (void);
// 0x00000275 System.Void DG.Tweening.DOTweenModuleUnityVersion_<AsyncWaitForElapsedLoops>d__13::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForElapsedLoopsU3Ed__13_SetStateMachine_m5E02C400A516339612E14DF2ED0B6046D90F6F47_AdjustorThunk (void);
// 0x00000276 System.Void DG.Tweening.DOTweenModuleUnityVersion_<AsyncWaitForPosition>d__14::MoveNext()
extern void U3CAsyncWaitForPositionU3Ed__14_MoveNext_m69E154F3EFA854C85D27C84F5C8624AE7595D01E_AdjustorThunk (void);
// 0x00000277 System.Void DG.Tweening.DOTweenModuleUnityVersion_<AsyncWaitForPosition>d__14::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForPositionU3Ed__14_SetStateMachine_mA77CBC54370E798D88ACF48F9D68586A41FC741E_AdjustorThunk (void);
// 0x00000278 System.Void DG.Tweening.DOTweenModuleUnityVersion_<AsyncWaitForStart>d__15::MoveNext()
extern void U3CAsyncWaitForStartU3Ed__15_MoveNext_m0BFB04DC7FE239554F4DF0B3DDD139E42A092F20_AdjustorThunk (void);
// 0x00000279 System.Void DG.Tweening.DOTweenModuleUnityVersion_<AsyncWaitForStart>d__15::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForStartU3Ed__15_SetStateMachine_mAD7263100F3F3DBD57295879534C47061409AEB0_AdjustorThunk (void);
// 0x0000027A System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForCompletion::get_keepWaiting()
extern void WaitForCompletion_get_keepWaiting_mCA7642C5A8C37F8C0A2CAE990CE1CB6AEE8FD2D9 (void);
// 0x0000027B System.Void DG.Tweening.DOTweenCYInstruction_WaitForCompletion::.ctor(DG.Tweening.Tween)
extern void WaitForCompletion__ctor_m818111A77A3380EE626346FE03A1A604BB896A1A (void);
// 0x0000027C System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForRewind::get_keepWaiting()
extern void WaitForRewind_get_keepWaiting_mB480319CB28155CA977F94C7FA03EE5353AA1285 (void);
// 0x0000027D System.Void DG.Tweening.DOTweenCYInstruction_WaitForRewind::.ctor(DG.Tweening.Tween)
extern void WaitForRewind__ctor_m66B575E497C363CB5137629B4D6A00D13B7CD5AE (void);
// 0x0000027E System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForKill::get_keepWaiting()
extern void WaitForKill_get_keepWaiting_m7979151F1AD842D2E8004FE37A2C51B47AB36647 (void);
// 0x0000027F System.Void DG.Tweening.DOTweenCYInstruction_WaitForKill::.ctor(DG.Tweening.Tween)
extern void WaitForKill__ctor_m1C9624CE32A1C83CEA14BB5EADD587B6AD79D829 (void);
// 0x00000280 System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForElapsedLoops::get_keepWaiting()
extern void WaitForElapsedLoops_get_keepWaiting_m6F7A59CCCC45BBA5125C6FC7AB667CD24359E8F4 (void);
// 0x00000281 System.Void DG.Tweening.DOTweenCYInstruction_WaitForElapsedLoops::.ctor(DG.Tweening.Tween,System.Int32)
extern void WaitForElapsedLoops__ctor_m8E720B450DD1350EE81EC3CCB5B6280BE5C51D8B (void);
// 0x00000282 System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForPosition::get_keepWaiting()
extern void WaitForPosition_get_keepWaiting_m34DFE8356EAFEE916828BFAF4A17A822B47AD687 (void);
// 0x00000283 System.Void DG.Tweening.DOTweenCYInstruction_WaitForPosition::.ctor(DG.Tweening.Tween,System.Single)
extern void WaitForPosition__ctor_m94DD0A05EF293B8AA83F343A12015C107AF7FDB8 (void);
// 0x00000284 System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForStart::get_keepWaiting()
extern void WaitForStart_get_keepWaiting_m59700AA1AB726A22C76BFAA0C52FCA460F6E337D (void);
// 0x00000285 System.Void DG.Tweening.DOTweenCYInstruction_WaitForStart::.ctor(DG.Tweening.Tween)
extern void WaitForStart__ctor_mD7AB17A603CF22568EEF0D9861C49F6CFD632284 (void);
// 0x00000286 System.Void DG.Tweening.DOTweenModuleUtils::Init()
extern void DOTweenModuleUtils_Init_mFF2188F42FA7128FE1A9BBC2CB9E6351919C9D15 (void);
// 0x00000287 System.Void DG.Tweening.DOTweenModuleUtils::Preserver()
extern void DOTweenModuleUtils_Preserver_mBE8D1FE2AE913FBFFCCF43B5A615D941B3706F43 (void);
// 0x00000288 System.Void DG.Tweening.DOTweenModuleUtils_Physics::SetOrientationOnPath(DG.Tweening.Plugins.Options.PathOptions,DG.Tweening.Tween,UnityEngine.Quaternion,UnityEngine.Transform)
extern void Physics_SetOrientationOnPath_mCC376173A621DA244564EDF8A6347AB2A0F47816 (void);
// 0x00000289 System.Boolean DG.Tweening.DOTweenModuleUtils_Physics::HasRigidbody2D(UnityEngine.Component)
extern void Physics_HasRigidbody2D_m86FAA0450979B8AFE6A9EF5E27837387C57765C1 (void);
// 0x0000028A System.Boolean DG.Tweening.DOTweenModuleUtils_Physics::HasRigidbody(UnityEngine.Component)
extern void Physics_HasRigidbody_m3082D72A658CCDDF6BB1F3D4A3EFBD6397D6E862 (void);
// 0x0000028B DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModuleUtils_Physics::CreateDOTweenPathTween(UnityEngine.MonoBehaviour,System.Boolean,System.Boolean,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void Physics_CreateDOTweenPathTween_m94D41B65E37500E5ACCF6B6F3B5D53FB031C4245 (void);
static Il2CppMethodPointer s_methodPointers[651] = 
{
	BackButton_Start_mED8FAF7A752B3AD143F9F0C563F82CD1F13C7194,
	BackButton__ctor_m7F3E5F7BC80AEB42857907CEEB4E70C95F7ADD74,
	U3CU3Ec__cctor_m777FC6B3355E4BE0CF900D7BC9FAAFCDA5D75C50,
	U3CU3Ec__ctor_mE52D3914667235EC01164521FDEFC27582585D57,
	U3CU3Ec_U3CStartU3Eb__1_0_mCFB87C3E8101789B3DE2882E7588033AFC711257,
	Box_Awake_m6804B828DFC994FBB4B5F7633C167831486AAEA0,
	Box_Start_m303F32963DB378B8D1424990FB77B8667550EB60,
	Box_OnPointerClick_m7C9A78825F697B96F4FBB098D454056869BD9D1D,
	Box_OnPointerEnter_m73280582AB84000A80E302D85CA4E3374F363625,
	Box_OnPointerExit_m4A7036E9052550B7C0639EF75C2E1C062789851C,
	Box_GetColumn_m836860B15F42088D3245D2C65AA3FF02EFDC3080,
	Box_GetLine_m00CE6139F0ECCB8ED23D4C29AF8B7D50EEA6813F,
	Box__ctor_mFDB4D9912FDCE47BA2A62DB77AD92F62989E0295,
	EntriesManager_Start_m53F5C106BC3BC8294A14DB225E5179356C62378F,
	EntriesManager_UpdatePlayersNames_m37FE34EBC83D16045DC5435D1F03191515543D50,
	EntriesManager__ctor_m66B0155629BDFEA05CC6FBE07A7B5FB6B57A5B7C,
	EntriesManager_U3CStartU3Eb__7_0_m13A7D85BF8A16BC5E04F913612EC66A365068DFB,
	GameplayController_get_CurrentPlayer_m64DF09B483531D2BC8638B3A7EB6BCF92DDA6BBF,
	GameplayController_set_CurrentPlayer_m23C87603DDF92A9D64EE179D545B034A63E1E9ED,
	GameplayController_get_P1_m4B6EA2B345DFBD9100D07F604349BB972E850A28,
	GameplayController_set_P1_m31F415A90BA634193384075424AF60ADDFDAF558,
	GameplayController_get_P2_m7C6E95B4966050C54211A66121ADFC88FCC06698,
	GameplayController_set_P2_m28254E7140A52B43190FE4E794C5FA7836C2A5E6,
	GameplayController_get_GameMode_m25FA4D34FA402DEA22F8F9053DE278F2538BBFF6,
	GameplayController_set_GameMode_m02060946BF0F509B79D717DE7E7035C7FD21544B,
	GameplayController_get_AudioSourcePlayerOne_m4F0007CE23F636247A334B79305ABE9501245DFF,
	GameplayController_set_AudioSourcePlayerOne_m5ED5E3B95B1BF8C3668D11AAB59643982BD8E8BC,
	GameplayController_get_AudioSourcePlayerTwo_mC83C9D57B149A3F3C55C72BEFC405E43DFDFB431,
	GameplayController_set_AudioSourcePlayerTwo_m38B3D5676DA9A299AA3652EF490ACBDFE4DF0EE8,
	GameplayController_Start_m00F87B1BD1027249FB804267BEF17AC4E5D4BC08,
	GameplayController_Initialization_m33290712194E9FF2663E23116A4E5F80D36964DE,
	GameplayController_EndTurn_m00C5D392D0EC03867821D2EA7E48460F5F135642,
	GameplayController_IsGameOver_m4A5FFE2AB7F518192334A9F33EAC7BCBAFFCE7FA,
	GameplayController__ctor_m6E7B42579EA00C13984BF45D41FDDCA3C58916F6,
	GameplayController_U3CStartU3Eb__37_0_mA35BB044D7E6E063A38B5C9EB34C2D6C9477E4A1,
	GridEffect_Start_m2D58A6254DC64C552C0CBF46FF7DE0F94787858F,
	GridEffect__ctor_mB7B3CFA091AE49B9CE9AF379FBC3E027BCFC92EB,
	Player_get_IdPlayer_mBD433887F4B838E2FC35BEE4AE32EFE0B3AB6FF6,
	Player_set_IdPlayer_m1CF2961F80F874ADE45749BECFA2F7057F09F4AA,
	Player_get_Sign_mEA0B0257A2DCC18B4E3357DE7689841E0FAF677E,
	Player_get_Score_mAD918B21E96F09C922D7D98E9F76FBD3505960B0,
	Player_set_Score_m3553298246BA3DB3698209F34278DB2D752427CC,
	Player_get_Name_m759A894136CA994C1A36BBD5E10F8D6FDF488BE8,
	Player_set_Name_mF5B95A6563BFFB722973440BFA83AE2EE1DCF41C,
	Player__ctor_mD67D4225E11A8F52CC815E852AB9F677F93E59CB,
	Score_get_NamePlayer1_m4C79819FBFEC9B0AD22A8A1CE9D712537E243BCE,
	Score_set_NamePlayer1_m19613351986F26A3C04678707CF45CC42DB1EC25,
	Score_get_NamePlayer2_m532B35B0A10FA2A91B5830181401C36251B272E5,
	Score_set_NamePlayer2_m0FB48D3C338FFE5327011615C400A2D761A1A3C5,
	Score_Start_m65B0DAC30D1A9E246D4CF14672D8138B53EDA564,
	Score_ScoreUpdate_m60681E317A27078F33EB487E7DD61DE167EEEA97,
	Score_UpdateDisplayedNames_mD5E2346E01EAB7F4C0D301C940E52726E3393E33,
	Score__ctor_mEE9186D20D9B28A735262B29AB6E8D9FF1380FB6,
	StartButton_get_GameMode_m98C915DDB3138681442B6E8B57371DF6ACDFF27D,
	StartButton_Start_m73507426F11BB8A54FE7939A7CBC7D4D0F78E27E,
	StartButton_LoadMainScene_m262E9A17B5D5530BA6ED4502B1C69F5396DAE9B9,
	StartButton__ctor_m6DAB2093EB360099E446CA6B746441526C0C7BD9,
	StartButton_U3CStartU3Eb__5_0_m39E06A9C5B023E0C5092F1B00C1BD84223983715,
	StartButton_U3CStartU3Eb__5_1_m024F8B089B8F8B4F3747B1CF5B7BE99DB88580DD,
	U3CLoadMainSceneU3Ed__6__ctor_m9ED86CF7C2035A18EDE99E643B6EBF661738CB02,
	U3CLoadMainSceneU3Ed__6_System_IDisposable_Dispose_m99CAEF5C5CD4D205BF82D247C097D399BB49D8DA,
	U3CLoadMainSceneU3Ed__6_MoveNext_mE08BEA0C1051A2019F7F3BF9A09F215811E5E408,
	U3CLoadMainSceneU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9E12BD1757333BC11838D94206F57B95C3B9BC3E,
	U3CLoadMainSceneU3Ed__6_System_Collections_IEnumerator_Reset_m78D8FDD7095F220C7657A3CB7474666B317DF797,
	U3CLoadMainSceneU3Ed__6_System_Collections_IEnumerator_get_Current_m5083C604160556C02DBAEDD48AA9AA3953343B13,
	ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2,
	ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B,
	ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371,
	ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026,
	DropdownSample_OnButtonClick_m97641451A2EAC4FC6682897A948A37FDD6BF9EA6,
	DropdownSample__ctor_m87664E3DC75A1A9AA19FEE6ED6B993AB1E50E2B9,
	EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93,
	EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE,
	EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC,
	U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B,
	U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF,
	TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226,
	TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E,
	TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1,
	TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3,
	TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385,
	TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4,
	TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B,
	TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788,
	TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A,
	TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076,
	TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D,
	TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA,
	TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB,
	TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5,
	TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70,
	TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11,
	TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC,
	TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44,
	TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC,
	TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53,
	TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66,
	TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43,
	TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77,
	TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8,
	CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7,
	SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2,
	WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C,
	LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018,
	LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0,
	Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C,
	Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F,
	U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73,
	U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E,
	Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4,
	Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376,
	U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651,
	U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561,
	U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2,
	Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046,
	Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5,
	Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77,
	Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516,
	Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE,
	Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9,
	Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C,
	CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E,
	CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610,
	CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373,
	CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3,
	CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124,
	ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA,
	ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3,
	ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC,
	ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8,
	ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2,
	ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469,
	ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64,
	U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B,
	U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362,
	SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F,
	SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5,
	SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697,
	SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB,
	SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F,
	SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3,
	SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3,
	SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86,
	U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF,
	U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A,
	TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4,
	TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB,
	TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12,
	TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3,
	TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720,
	TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21,
	TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1,
	TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C,
	TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF,
	TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A,
	TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D,
	TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0,
	TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B,
	TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363,
	TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C,
	TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B,
	TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE,
	TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943,
	TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD,
	TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0,
	TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66,
	TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF,
	TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A,
	TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB,
	TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910,
	TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691,
	TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D,
	TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F,
	TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F,
	TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2,
	TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33,
	TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E,
	TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944,
	TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647,
	TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84,
	TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB,
	TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF,
	TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE,
	TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7,
	TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF,
	U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9,
	U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261,
	TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0,
	TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA,
	TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50,
	TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712,
	TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8,
	TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534,
	TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4,
	TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848,
	U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C,
	U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F,
	U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8,
	U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27,
	TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046,
	TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF,
	TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E,
	U3CDisplayTextMeshProFloatingTextU3Ed__12__ctor_mA27BBC06A409A9D9FB03E0D2C74677486B80D168,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_IDisposable_Dispose_m510FB73335FCBCBEC6AC61A4F2A0217722BF27FC,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_MoveNext_m8DFB6850F5F9B8DF05173D4CB9C76B997EC87B57,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCD7B5218C81C0872AB501CD54626F1284A4F73BF,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_Reset_mD2D32B94CA8D5A6D502BA38BDE1969C856368F73,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_get_Current_m59B2488B0E3C72CE97659E8D19BB13C6E0A44E90,
	U3CDisplayTextMeshFloatingTextU3Ed__13__ctor_m359EAC129649F98C759B341846A6DC07F95230D2,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_IDisposable_Dispose_m36DE8722B670A7DDD9E70107024590B20A4E0B18,
	U3CDisplayTextMeshFloatingTextU3Ed__13_MoveNext_mB7D320C272AAA835590B8460DA89DEBC0A243815,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m891591B80D28B1DF2CF9EB78C19DA672A81231A2,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_Reset_m5EF21CA1E0C64E67D18D9355B5E064C93452F5B2,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_get_Current_m4850A72D887C4DA3F53CA1A1A2BEAD5C5C6605A1,
	TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB,
	TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA,
	TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1,
	VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC,
	VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66,
	VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42,
	VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9,
	U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52,
	VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F,
	VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825,
	VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846,
	VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97,
	VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549,
	VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16,
	VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE,
	U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF,
	VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2,
	VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA,
	VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2,
	VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE,
	VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E,
	VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599,
	VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62,
	U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E,
	VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5,
	VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971,
	VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4,
	VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1,
	VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5,
	VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87,
	VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC,
	U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD,
	VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C,
	VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC,
	VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884,
	VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C,
	VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F,
	VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED,
	VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2,
	U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107,
	U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F,
	WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0,
	WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4,
	WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB,
	WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2,
	WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099,
	U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60,
	U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25,
	DOTweenModuleAudio_DOFade_mEA99A941484D71522BCDD1CCC43B790A6AF4A0B3,
	DOTweenModuleAudio_DOPitch_m6512EC4BEE6C70E110DDFC1048BFCE13FA87E914,
	DOTweenModuleAudio_DOSetFloat_m8831E89C2E951DDAC9C20A46D3AAD329BAD9C09C,
	DOTweenModuleAudio_DOComplete_m49C5BD0D315241E09EA8D6398F8011C5A34646AE,
	DOTweenModuleAudio_DOKill_m2CED6088E868165A2D91768409FAFFD980993747,
	DOTweenModuleAudio_DOFlip_m080FDDE3ECDF68901289FBC6EA1297EF278D9D36,
	DOTweenModuleAudio_DOGoto_mD4A9A702C6187927FDBDD3220570820533459D16,
	DOTweenModuleAudio_DOPause_mD42B2DFA89EA1635A6F9E813F7B852C852F282E5,
	DOTweenModuleAudio_DOPlay_m95942072A27A597B1FF4FA58A56EC35EC3A8DC15,
	DOTweenModuleAudio_DOPlayBackwards_mA2814EE20AB3527ADC90DAC391BDF4AD2DA63104,
	DOTweenModuleAudio_DOPlayForward_m808D504EA5FCA7B43292D4024735C69339BEC168,
	DOTweenModuleAudio_DORestart_m53CC4DB3E3B69233410704AC28B0E7E5FE15F9B6,
	DOTweenModuleAudio_DORewind_m1102048CF3E759CC5A0B7AF48B37092DD8625D5E,
	DOTweenModuleAudio_DOSmoothRewind_m4C703A054EDA680CDA0FE09924AD97775BF70DA1,
	DOTweenModuleAudio_DOTogglePause_mA1AE6D3A3A8FA9692CE0695E915AA08626EF61CF,
	U3CU3Ec__DisplayClass0_0__ctor_mB8ECD32BCDE8AEE5DA844CB16FB319546FDF65F4,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m6447B544882FDCCECF5F03C92CFD462E52F55C3C,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_mCBA7DC56068D62779FD9318BF1BF6CC04674AA73,
	U3CU3Ec__DisplayClass1_0__ctor_mD593A9B61B5F03AF9D1695ED9FA2C1E993F41739,
	U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__0_mB6B65C2ED3194137382A4DC81F72AB5C2C95FEB7,
	U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__1_m9ED5ED10167C9FA210B584AAC22893154A683518,
	U3CU3Ec__DisplayClass2_0__ctor_m3D650640D73868903AB5235513EB20B1EF57851E,
	U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__0_m1B04734B7D8B9A386220DCDED11506098EDF5039,
	U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__1_mDE42A0D8DA684D1F5E21CA1797A119D6BF6252C3,
	DOTweenModulePhysics_DOMove_mAD664CEB3F56A94F3481FA83FC799F0D7F8FF53F,
	DOTweenModulePhysics_DOMoveX_m4CD527117E19DE2009884655A131DF83B9EAD4A9,
	DOTweenModulePhysics_DOMoveY_m126BCA31B85BF67D1B5E85549A95802B69777A2C,
	DOTweenModulePhysics_DOMoveZ_mDC3444100D64651B456A118C92B6F77C99C32597,
	DOTweenModulePhysics_DORotate_m2BDE8501CE323175A7471D694249E660F5BF068C,
	DOTweenModulePhysics_DOLookAt_m706C3BF65EDBCCB2562FB115E61151945941C5FC,
	DOTweenModulePhysics_DOJump_mCDD95D56D93498D68A345E57B3C359388A816821,
	DOTweenModulePhysics_DOPath_mF69DF997EF91CD5347391BC89E6CE268A5CD39DD,
	DOTweenModulePhysics_DOLocalPath_mB056B81341D5048E516138EBE4A680B3F4664D6D,
	DOTweenModulePhysics_DOPath_m3C49FFEB71D494F474F7223248D7105E90CF5BE7,
	DOTweenModulePhysics_DOLocalPath_m6F939DAF66D56DA9B52DA62546C70F3862E14193,
	U3CU3Ec__DisplayClass0_0__ctor_mCFBBC383950DE52D74C6F4E85C4F7F3273B10719,
	U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_m8158605D880C075EEDC090F3BD1175AA33E2E107,
	U3CU3Ec__DisplayClass1_0__ctor_m4E41921AB96267B39AAB22B0790884BEB58B467D,
	U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_m0EEE1A4D5C020E63505FB9F7E09A531433FC817D,
	U3CU3Ec__DisplayClass2_0__ctor_mA7A465D617CE4C839F719F65F44B2D806976FC71,
	U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_m4930252334FB6162D77F0821409D7487A96CB696,
	U3CU3Ec__DisplayClass3_0__ctor_m9E94ECE0838CF218AB657F8C4B51F881B310EF34,
	U3CU3Ec__DisplayClass3_0_U3CDOMoveZU3Eb__0_mF75D2479FE18CCE0BFC07AA4A06B1EBE01369D29,
	U3CU3Ec__DisplayClass4_0__ctor_m1A01BE964AEBF9737E72E7C9B3B5D660D965B936,
	U3CU3Ec__DisplayClass4_0_U3CDORotateU3Eb__0_m6B41E1A9419EDB0B9FDE8C9A62BDAA1281D19EF5,
	U3CU3Ec__DisplayClass5_0__ctor_mF63278B7093557C5BF31EE7878AC0E87D82EEC74,
	U3CU3Ec__DisplayClass5_0_U3CDOLookAtU3Eb__0_m2019767096427977C234F4A6698E6C2559840795,
	U3CU3Ec__DisplayClass6_0__ctor_m06AC3FA2094299E1FDE7ECAFCC8F970C5139B4FE,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__0_m847B6B84B9DBBB179602A2DAC20A1E47B7CD0C6F,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__1_m488237F614033272B64730F763C4EFCD4009ACDB,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__2_m6F586BA8E7E757466B5E77AB61BD0056F4D86D28,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__3_mEC9264AAF5B45E16B176AFA00079BFDA173EDDFE,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__4_m15CD7F1F9A30E34BA9AC943FA510B9FF0C15F84E,
	U3CU3Ec__DisplayClass7_0__ctor_m177CD3D0B0169C7DF1FD6F64FB8C39D0F92E7023,
	U3CU3Ec__DisplayClass7_0_U3CDOPathU3Eb__0_m448E92A163A6A98FFE561134611FAD6418C6824B,
	U3CU3Ec__DisplayClass8_0__ctor_m4499D3AB924EEB9EB850233A8F64E5EC4A7AEA3D,
	U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__0_mAB235341BB5079CA397BF09B075B856C6B950EDF,
	U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__1_m19CF06E0D31E737EFDEEFE6D4D2361EA0D372730,
	U3CU3Ec__DisplayClass9_0__ctor_mCA1E6F241784A13DEC15E12A0219479D552BB6BB,
	U3CU3Ec__DisplayClass9_0_U3CDOPathU3Eb__0_mD050544C13BA54260FC94E0964EF204B4EDB2628,
	U3CU3Ec__DisplayClass10_0__ctor_m819824BC9854A0C90662B04D48B88CA7B052BF83,
	U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__0_m3089CA33F25546258A5E846EA51421D71AC55D70,
	U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__1_mD201EFBB4403FBDEC0D377D39A768A581E6CCDDD,
	DOTweenModulePhysics2D_DOMove_mC7C9AD67230F89E53BCC36B37BC699E6E3DBFCC9,
	DOTweenModulePhysics2D_DOMoveX_m47B42CE055F1892B75820F6FDE992E541394D23E,
	DOTweenModulePhysics2D_DOMoveY_mBFBABC3C73C6D858C280B8E0E21869BCFC57938F,
	DOTweenModulePhysics2D_DORotate_m8E6DBA35EFB07C71A288D36A86B1299D99D2BE6A,
	DOTweenModulePhysics2D_DOJump_m13774D66ED37FC3FF7BA6862C306D32C0B01C351,
	DOTweenModulePhysics2D_DOPath_mC8E35580B3D00EC82A6EB5150838D2D220978092,
	DOTweenModulePhysics2D_DOLocalPath_m56F836B516F2576C6CF48E07518EF403ED233844,
	U3CU3Ec__DisplayClass0_0__ctor_m28BD574ECE1FAC2A8040F507B4347CA641B883C8,
	U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_mD0BB74EE7E3ED3D15C2B501656DD41CB4DB1CFB0,
	U3CU3Ec__DisplayClass1_0__ctor_m0A358EE93B5CBA3BAAFED5EFA3CB29B852CC0A8C,
	U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_m890A07E5B25CF040E903014587C8C30FC43505CF,
	U3CU3Ec__DisplayClass2_0__ctor_m79644C3E99A1BADC2CF695A33996EFBE999BCA28,
	U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_m833E99A0F3AFA6D89F501FE08B0EF7371AE20D4C,
	U3CU3Ec__DisplayClass3_0__ctor_m37C53B07CACF2CE16A6FECB187B45E328882E6CC,
	U3CU3Ec__DisplayClass3_0_U3CDORotateU3Eb__0_mE8799B2B2CD516708D2235A0D1822AF9AEA117DF,
	U3CU3Ec__DisplayClass4_0__ctor_m35FA34381650BF0B9A9C29EE83B8043082D0A482,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__0_m66684B2056E4B92E933AD98C2EAD6EDD9AF9C174,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__1_m99B395C93C8827215768541DBD48AAFCAA4014EC,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__2_m9D2C7FBC51C4DBCCD91E8359A6FCD6D5BD633FB7,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__3_mEC32920DB355DC7E1AAF2249C33F3177670806E8,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__4_m053F71A269824B99AA058986F6E772F950C453C7,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__5_mC21E068A8BCD02C771D362D4397E7C1113CC2E3D,
	U3CU3Ec__DisplayClass5_0__ctor_mC0712263E288AF8337D78ACD1B4187EC2A79507D,
	U3CU3Ec__DisplayClass5_0_U3CDOPathU3Eb__0_m2DD131B5B01242CEE25D2865DF2F0BC59A4753B0,
	U3CU3Ec__DisplayClass5_0_U3CDOPathU3Eb__1_m80D3EF48ACDCE8FCE54C12A18D6A47AEC649ECBC,
	U3CU3Ec__DisplayClass6_0__ctor_m56FEA3991E5AE4A9842780C1DAFF6AF473C21946,
	U3CU3Ec__DisplayClass6_0_U3CDOLocalPathU3Eb__0_mC6200A1DCC1B49838E2F65C06843DA00BBBA220C,
	U3CU3Ec__DisplayClass6_0_U3CDOLocalPathU3Eb__1_mA1E8B523414CEE79D40909E7D32EE7347C0E7A8B,
	DOTweenModuleSprite_DOColor_mA67D7B77174B479F8AE20F739A74BA7A2BBC9F0F,
	DOTweenModuleSprite_DOFade_mEED04CEE3EE9D32B87478A3CFDB9CFB710307769,
	DOTweenModuleSprite_DOGradientColor_m47670ABD64AEDC972E8210CAF4C8A778B0B7676D,
	DOTweenModuleSprite_DOBlendableColor_m51BBDB4A69B0560481736E6734EA2BF52DB1537D,
	U3CU3Ec__DisplayClass0_0__ctor_mD77E24E24423ADEC43F9983504BD6796D2671E99,
	U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__0_m12BAC3674340BDF408F41A0E9BEEF6BB87C3F504,
	U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__1_m89E34AE3BBC636BD2B38C1AB7300F1F20F25E616,
	U3CU3Ec__DisplayClass1_0__ctor_m40F18C57ED175BA68D1D65FFA97879AEAF2E3325,
	U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__0_m8B9E647B54E5DA35EE71D65866372C8C90B7F452,
	U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__1_m935CE2E2B7CB25D7151DA0BF3B4420C8099E0A45,
	U3CU3Ec__DisplayClass3_0__ctor_mB0AA92A12DE97E8954819AC2BAD916065D4BECDB,
	U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__0_m371A7E7E53DB5FC70A6534A7956B27A3A8B7EA03,
	U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__1_m36515014C810F349D12C86DC636349E452DA6974,
	DOTweenModuleUI_DOFade_mF927A1EDCA91814D8A466A343E3223EEF4ED7478,
	DOTweenModuleUI_DOColor_m7067683474EE6A600294C2CCFD941403FA789A6E,
	DOTweenModuleUI_DOFade_m41638D05EB7678C523D0D39E320FAC80B8E48658,
	DOTweenModuleUI_DOColor_mF4B41443760536F3C399612AD0283A52937D95CC,
	DOTweenModuleUI_DOFade_mE66C8B7D137E0DDE54AAA96F551811FB8BDB8754,
	DOTweenModuleUI_DOFillAmount_mE4ED52B826A6F8035320E980F7C701B4047C4DBA,
	DOTweenModuleUI_DOGradientColor_mB4B8D1DEDA92464670EB8A6D985973477D51A4D5,
	DOTweenModuleUI_DOFlexibleSize_mF0916C4E7A906202EFBAEA89CC2B55A9C3CF48B3,
	DOTweenModuleUI_DOMinSize_m54A6919625499AFD6F801C6EC9672965DBDC96E1,
	DOTweenModuleUI_DOPreferredSize_mE26AFB2C606F095483EDED449FB8A5A80C08A8DE,
	DOTweenModuleUI_DOColor_m15BC0FCF035E3DEEC3CFC30465EDE0FE6FD6D2EA,
	DOTweenModuleUI_DOFade_m86DB80308D92FC4DFD3E981F9E958FF2A207EA74,
	DOTweenModuleUI_DOScale_mFA27F29E3DFB6B1CFCAC3CE73F06DF3737A3EB06,
	DOTweenModuleUI_DOAnchorPos_m63999D434CE7E8F6BE66BBA92532DE9D98DC9F79,
	DOTweenModuleUI_DOAnchorPosX_m9B57E02E9902C7A3F1A8DD1021AF630C10478380,
	DOTweenModuleUI_DOAnchorPosY_m14CCA63E545D7C28AD1A76488D0084F55EE94CA8,
	DOTweenModuleUI_DOAnchorPos3D_m6DEB7C4AE4CD2118BF83D268945340F3373DC7BB,
	DOTweenModuleUI_DOAnchorPos3DX_mA40303AC769280BDE74EE58BE5AD80BEA427CE08,
	DOTweenModuleUI_DOAnchorPos3DY_mE6BE56AE421B959F22CBA6318BABC3CD367A7277,
	DOTweenModuleUI_DOAnchorPos3DZ_m702F7D4FE9DC8E6116FB5E0DD478D2BB88ED1242,
	DOTweenModuleUI_DOAnchorMax_mDD97F6A8B4468622E3808507CB6B89A8A109BDCE,
	DOTweenModuleUI_DOAnchorMin_m60C4775B1EEDBCF2CD3E96A8171C8A83FD7F400D,
	DOTweenModuleUI_DOPivot_mF623939EDCC152F664FECC8B71E321B0AA13F0A9,
	DOTweenModuleUI_DOPivotX_mC21C031A61BDB7C3A8087719F23CBC6D1D762D6B,
	DOTweenModuleUI_DOPivotY_m9922C28ADECA433362D3C6C816318CA00705996A,
	DOTweenModuleUI_DOSizeDelta_mC4FD07525E1CA5BBF577030D571CE0CCEFD4E6B7,
	DOTweenModuleUI_DOPunchAnchorPos_m058E3DA140B9C497727F05AEAA296425E585B9EA,
	DOTweenModuleUI_DOShakeAnchorPos_m89D1C74E940B9434B2234FB49C031AD8FD1170AB,
	DOTweenModuleUI_DOShakeAnchorPos_m7818867062313A535AB2CD6F86267E2AB9357AC6,
	DOTweenModuleUI_DOJumpAnchorPos_m7046EE1C82AF0159CFAAA460E4C3D22797625480,
	DOTweenModuleUI_DONormalizedPos_m300F112FE2F67D4DC718083340C7FD27BC96F248,
	DOTweenModuleUI_DOHorizontalNormalizedPos_mCB4BCD85B49C458BE871F890A86FEFE0D7F41B42,
	DOTweenModuleUI_DOVerticalNormalizedPos_mC0269B4644816A0D5C09717CA92E4229CB31CDF1,
	DOTweenModuleUI_DOValue_mFDF45D60F52C82AACD8B50859A51B8F315A44F08,
	DOTweenModuleUI_DOColor_mFD36CA1EA683A0D2312EE87BCF3B0A924504E797,
	DOTweenModuleUI_DOCounter_m6C87AB89E5B14F1FB82630CFF15791678D7DF055,
	DOTweenModuleUI_DOFade_m21F7CCBB732ABBD63BFD893D1A8D9837532A70D1,
	DOTweenModuleUI_DOText_mC0932E45473278B2FFB1698954CC0EB0422C626F,
	DOTweenModuleUI_DOBlendableColor_m4C5E9A68BFBEE876E1FF8D8E9A4BA77856F144C1,
	DOTweenModuleUI_DOBlendableColor_m18196516A24780A550314E9327868D6D6F3EAA4E,
	DOTweenModuleUI_DOBlendableColor_mC30D1F805FE976FB25EC361AB0ABA94413361B6F,
	Utils_SwitchToRectTransform_m260A15449F1C7A8F4356730DF4A59A386C45D200,
	U3CU3Ec__DisplayClass0_0__ctor_m14E6397A137D3CB8A7638F9254B4694553B4DC7C,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m6F0CD242FAEF332D726AD4CE2EB2691CCB3748B1,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_mDBDE14547A6DF70C0ADB82403282C26FBB4F6A27,
	U3CU3Ec__DisplayClass1_0__ctor_mCB63EFBBE399EEB9892054E0FAB2AB42E2BBB886,
	U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__0_mE8D0790E2303D84F8D513854D9F004E5E3127A3C,
	U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__1_m07CB6A64D93955BC07E8AE290B01F9F39195A5A9,
	U3CU3Ec__DisplayClass2_0__ctor_m61C588C0764BA908B02EBAB1C19076EBB2898A9E,
	U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__0_m85CC4106263DD670C5BF648B8A64C6BA84297B65,
	U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__1_m140BDA0B825925A5051A2565C055CCBBF005A680,
	U3CU3Ec__DisplayClass3_0__ctor_m09228DABE56F6866E09591FE27C255A39A71E48D,
	U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_mA7BF7CEB8AA94694104E8D048F5045D122A5D980,
	U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_mBC7108789E76320E9507A9087273C7D89227F0F3,
	U3CU3Ec__DisplayClass4_0__ctor_m5CD451B862F3CC71D8D25BC3A69725FCDD6144C4,
	U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m507CD55516001BB4A76D7E58CB159B5394D6413C,
	U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_mE65F1E4D380EC305AF0AE93E0315B0B23CA36310,
	U3CU3Ec__DisplayClass5_0__ctor_m879B3843E0C43157D8044AD52E2F865EE50FD041,
	U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__0_mAFE3629BC44AE61C84399809F742CEF0235004E1,
	U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__1_m9EB6C21BCE8D1A08FF84815C05A6664637C510CE,
	U3CU3Ec__DisplayClass7_0__ctor_m47B2E10FFFD9FCACC88586DEF6A4E675DC9EC406,
	U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__0_m7A7EA5F66922413743B8C1AB4234645A32098EA8,
	U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__1_m0E76B7C6F893FD2F7F24E302CB55882765DD1153,
	U3CU3Ec__DisplayClass8_0__ctor_m0953E12771619B6F14FC14CA853AF5CA110A0553,
	U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__0_m9BD9F63B884498F9AA02C307EB1CA15FA29BD094,
	U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__1_m546E530F9DE46CA1359450860E62DCC3BD3D72B3,
	U3CU3Ec__DisplayClass9_0__ctor_m3EB28823EF4D152A06983986C8120490212A8DF7,
	U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__0_mB87EAB728455B68D07773FA10DCA169804482CBD,
	U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__1_m24617F67F48227A961C0543BF7925F8D2F2A90B6,
	U3CU3Ec__DisplayClass10_0__ctor_m11212BA4C7EB97E0EE079E78FA1C95C02D517C75,
	U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__0_m08737F267CCB60C0CA0244070DA3C50845FC8B1F,
	U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__1_mAFFD9D2D2F3FD2BE4E2423286098B9BCC1C0C193,
	U3CU3Ec__DisplayClass11_0__ctor_m9B3BB08FF1DC2F3101E0D580891268445B7763CA,
	U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__0_mA57545026A9AE0CB3A20B6FFCDCF6F2F1CDA6AB0,
	U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__1_mCC70305FC1C1B93A838838519D023AC608D3E23E,
	U3CU3Ec__DisplayClass12_0__ctor_m05F6F905C11A344C5681CE1CD406DE85940356E5,
	U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__0_mEE927D8596DDB335403416BF63FF4E7E27D49D51,
	U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__1_m65C60213296203590F596B6A3481E4B8621F96D5,
	U3CU3Ec__DisplayClass13_0__ctor_mC6D360265628BCFBFDD42CCAF1136983FDD186BE,
	U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__0_m693A41C7FCF87BDE6211DE0ADED05AA5C476D71F,
	U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__1_m0F8F9CA39BC8894705C1EE9983E59D24A7BF218B,
	U3CU3Ec__DisplayClass14_0__ctor_m3BB3CA37E5912C211B954D7EE850C77F0B04BFF6,
	U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__0_m02BA203D0A6C9D64BC8149C197099DA29370C872,
	U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__1_m47B6E3E511B7E234B49FDBD4D6BC32E5EC92B1E9,
	U3CU3Ec__DisplayClass15_0__ctor_m011D15F0644147EABAFA4ED415CD3F5E8782CCE8,
	U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__0_mCBF0F2691FBC7AED64284B0FB4CCD75792AAB51C,
	U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__1_m0E6D417FD9062B34D8C0E8B58C4C688B39CD9603,
	U3CU3Ec__DisplayClass16_0__ctor_mF4FDD3E32D1C9FDD48079A148410AAB0CF4855B2,
	U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_mFBE3FE1AC3F56C148108234A9A1285F87EEF50E8,
	U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_mFEF34D901D1E18E74D7E93297BDD75ABF146514C,
	U3CU3Ec__DisplayClass17_0__ctor_mEEC98B74C2A5EDE984A5A59F4289ADFADAB3F804,
	U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__0_mC8D86325D798D952B390643A8BAE1D995368D36A,
	U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__1_mAB78FA2C6EEC74D6E259B2ABF94ECBD3CB62DB82,
	U3CU3Ec__DisplayClass18_0__ctor_mE272C380064D4945F3C7FDC2357A8B765BC52841,
	U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__0_m8C53152ED72D09B8F2BE2DE14963561650D0C42B,
	U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__1_m9E1FC98A6DF461320AC4B6F294F1A69AFD058E3E,
	U3CU3Ec__DisplayClass19_0__ctor_mF4A0F29D8F3315E7DD0BC8103DA0023E16C341CB,
	U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__0_mD27E4DF91E21C64D13997EA12A7659B1E31AA77B,
	U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__1_mD6F74CB34BAEA91A512586538770AB27FFB68A1D,
	U3CU3Ec__DisplayClass20_0__ctor_mCE70C1CB9A605F65BE4D31D224111EE4C6FB7DE6,
	U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__0_mAB5724DF7472A7CED2070B75A1B0E12378D5E6DD,
	U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__1_m7E357679D17273D809A39CDF435B5E64E46A64A0,
	U3CU3Ec__DisplayClass21_0__ctor_m47D7A3B4DA7AE2FE2F83164406834CF9150E5308,
	U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__0_m4804C6D34689DD21311DA86CC22008D3B0774391,
	U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__1_m73EF1691143A0E7E0254A73F52FDC73BB5AAA3E6,
	U3CU3Ec__DisplayClass22_0__ctor_m0D4210D174301CE26FA7A519B4168E7D87F6D92A,
	U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__0_mB3D6F3AADE069EED625038A53491AFEA1DAFD9DF,
	U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__1_mDCA8630A9398A411ACB0950973AB99C0723CC074,
	U3CU3Ec__DisplayClass23_0__ctor_mA47954A6646E43342880718EE9B5E66A0D18FDF2,
	U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__0_mFC16CCA332D908526AC3DC2BD8B4456969000CF5,
	U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__1_m7E0DAFAB836AC4F133B995B50578D8911BA3113B,
	U3CU3Ec__DisplayClass24_0__ctor_m0524772292383368F9C9F6BDFF0A114D60A35F8F,
	U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__0_m959088FDF2C1CD6EF40EE35BA531719E60C5327D,
	U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__1_mCECF34E3A27F03AA2DB5719CA1241927DC99BC5B,
	U3CU3Ec__DisplayClass25_0__ctor_m1ABD5A4A91526CA156C55C23A54822DC898A34AF,
	U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__0_mF87E563BB15F601A91B0DA1012A1604A52FCBDA5,
	U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__1_m26B3B99EDB1D47D81931CC6CABFED8406BF4E90E,
	U3CU3Ec__DisplayClass26_0__ctor_mC4836A2FD37334971703D95C49ED35BCAD56AFB8,
	U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__0_m357B263CA209B3E6EBB7F95AC352AA8B28A5CFC5,
	U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__1_mE7127F5C6AB73DBA7D40F67FD61BA4076E2B8D23,
	U3CU3Ec__DisplayClass27_0__ctor_m07ADC405B65035A17DC97097DED9B4C1D21EFB17,
	U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__0_m1AF2B166591EEA50BEE7FD3C2E9B7EFFC0D39F8E,
	U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__1_mF7180DEAAB00C14DD6F3DB8B815D0C0B2DB64725,
	U3CU3Ec__DisplayClass28_0__ctor_m4969157094B09547B26225881BD87025C928B506,
	U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__0_mB4E05CBCDBD4732D154E1A64678339D3FC5FBCE9,
	U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__1_mBDA6378719836E14C155B4648BB47429C125590C,
	U3CU3Ec__DisplayClass29_0__ctor_m00E6EAFA57923B235554FA8A236AD64992F363FE,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__0_mAE0CBA3D52D03EB2F2C37D85C5832B412F816591,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__1_mB685B77262E778382FAF923C04FD328DC494426C,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__2_mB23D92AF70DCD33F838372FF59B1F39DD3775824,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__3_mB86E86358B9C1671706EA489B6FC30C3874224E7,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__4_m1E0D9286E08822DF086E86BA60F0EFF6B62A32C7,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__5_m6E6A274B0B852663D3ED8CDD2B4646B9D618E535,
	U3CU3Ec__DisplayClass30_0__ctor_mCC70550D8AD7B4F88C913940A644E81F11F5898A,
	U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__0_m92C7B1817960CE4D1D7333872D37C8AF131F68FE,
	U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__1_mFA97248E3617865FEF6C3F36E633266FF044050F,
	U3CU3Ec__DisplayClass31_0__ctor_m82279F879253DF896CCD126EC0B7E16B295D165A,
	U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__0_mB33367FCE1348BA9F855A9D1366FA19F5DDA4AEB,
	U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__1_mEBA7CDAA06332B1E56991D8E2299E24B9348D627,
	U3CU3Ec__DisplayClass32_0__ctor_mD93F08EDF6689C6BA223067191EE8C9E0FBE509C,
	U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__0_m83DD02084574CCFB9F26C19BDE6AF6DE2C6387FE,
	U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__1_mAE94A6E7E27147AA530D056E286A00C8EAE56331,
	U3CU3Ec__DisplayClass33_0__ctor_mB387BC26BCA1B460D5B3B1CE767228FA244B7703,
	U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__0_mBB603DBBDA542D7F00142102A667BCB9682EC145,
	U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__1_mA3DF76574D5E76992AF228A3C4AFFFC19D6BAE15,
	U3CU3Ec__DisplayClass34_0__ctor_mA88FD675FEF2971D3F8145822B844ECFFF59BF17,
	U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__0_m5DC2ED4C56B0A1D8CFCB8A4E2D2078EA79E8B66F,
	U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__1_m9BA1BADA179249B9F0AD1142BE826798FA114E13,
	U3CU3Ec__DisplayClass35_0__ctor_m5533AE6A72AB74A97282032790D0DE81F0EAF811,
	U3CU3Ec__DisplayClass35_0_U3CDOCounterU3Eb__0_m4E790A70F3934B1A2F06D5692F8550A69C2567A2,
	U3CU3Ec__DisplayClass35_0_U3CDOCounterU3Eb__1_m3207CE6E96DAC0D117EFA238D72BAC0FAEFB2FE2,
	U3CU3Ec__DisplayClass36_0__ctor_mE2912C6BF4318E28870DE7AA92E4B05C80D92EE0,
	U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__0_mA3238766629BBF7E2750683F62F7629C5B097D87,
	U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__1_m119575F5E470CA6D0C37EF9E1A9C3A2CF8B2AA3A,
	U3CU3Ec__DisplayClass37_0__ctor_m59CE3FBA46AF3F7C3597AD84DEC898DB9B84EE39,
	U3CU3Ec__DisplayClass37_0_U3CDOTextU3Eb__0_mDB021AB28041C55E82DF362F8DDF94879B7EBA13,
	U3CU3Ec__DisplayClass37_0_U3CDOTextU3Eb__1_m0724BC3E19FD8643DD8C0FF25D4C08196FB446DD,
	U3CU3Ec__DisplayClass38_0__ctor_mB3E91AEAFAE65DD5FC36DD530E74CE1F4FA24AEF,
	U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__0_mB976655AEC70D25B43AAAF577CC953C7EB03D2EE,
	U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__1_mF14436C4F4EEB5B7664C3444B969B694BC6E3E5E,
	U3CU3Ec__DisplayClass39_0__ctor_m1DB7B55A6B3E4B184E35968EACBF9E214E0CED7D,
	U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__0_mE93AC95325F69D83EE746421FA5992DC25009961,
	U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__1_m45DD6E7ED579816AC18DF6B88901D9F11DB1B36F,
	U3CU3Ec__DisplayClass40_0__ctor_m089AA2605E325970B89E7F1519CF4DD5676CA2AD,
	U3CU3Ec__DisplayClass40_0_U3CDOBlendableColorU3Eb__0_mD869D1DD9D45824831045C0E5638161837B0854B,
	U3CU3Ec__DisplayClass40_0_U3CDOBlendableColorU3Eb__1_m6B0A0EFCA7952B551FD8C87503133EA08D8075A9,
	DOTweenModuleUnityVersion_DOGradientColor_m7E9B0D004D4B3D602EBB852CA1755FE863C6B839,
	DOTweenModuleUnityVersion_DOGradientColor_mA4E191E86F2EF81966259A40B2CDADE375437920,
	DOTweenModuleUnityVersion_WaitForCompletion_m4BCEDFA5ED1ACF4BC1B3531C0C306A7AC553C1F2,
	DOTweenModuleUnityVersion_WaitForRewind_mD9F37D5AAD9B665EE07F083D026E65194A8E3E6E,
	DOTweenModuleUnityVersion_WaitForKill_m02BD7F3B018BB6B4ED4FCAF47B8DA435EC60BD6B,
	DOTweenModuleUnityVersion_WaitForElapsedLoops_m46D46788899029A20F73BE252F7427D853131404,
	DOTweenModuleUnityVersion_WaitForPosition_m1771625E294B38C5D13DCA40846AEA881C1B23E6,
	DOTweenModuleUnityVersion_WaitForStart_mB4AAFE83B50AEF3A167F31C14B2F58794527F905,
	DOTweenModuleUnityVersion_DOOffset_m09CA228EBED193314D3D9A41BBDC97393E1FC4D2,
	DOTweenModuleUnityVersion_DOTiling_m6A3CD06EC3030A93EC3E93DE678F34048D5C1141,
	DOTweenModuleUnityVersion_AsyncWaitForCompletion_m886CBFB2DCD9455C2762F639F065A7AC9F1EC2E5,
	DOTweenModuleUnityVersion_AsyncWaitForRewind_mCC0B2A591BA46DE9E5CB59EA84461051477D5642,
	DOTweenModuleUnityVersion_AsyncWaitForKill_m11E8731B8A8E89493FB7976A35DCF4DD85E2132B,
	DOTweenModuleUnityVersion_AsyncWaitForElapsedLoops_m9F3D38817E779AA9C5F0BA7811E6626A7BFBB5B1,
	DOTweenModuleUnityVersion_AsyncWaitForPosition_m6207EF50EF7DE8402EDC806F8FEF6CC313C4DB03,
	DOTweenModuleUnityVersion_AsyncWaitForStart_m637C3E8C05D413E080897E3A99CF21C4BD859F25,
	U3CU3Ec__DisplayClass8_0__ctor_m0EACBC706CDB7B8BCE4C21D3AD250FE25CD825CF,
	U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__0_m6D616025F8610086B30B83BC8ED13213DB2AC7CC,
	U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__1_m4171C9EB0226E0E06E574EFD507A8D352EC5B557,
	U3CU3Ec__DisplayClass9_0__ctor_m6CC810F5A3B1779C2569B21779361FD5F94C2C9C,
	U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__0_m747CC91BE7EDF7D2644045B72BF689A2552B855F,
	U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__1_m0D5FC9115C0FFE113CA2277BA5A17562550B19F6,
	U3CAsyncWaitForCompletionU3Ed__10_MoveNext_m7A14EB02262CB797C27664F35565B88E8BA060D3_AdjustorThunk,
	U3CAsyncWaitForCompletionU3Ed__10_SetStateMachine_m85F5E4DAF7F7E7AB65762F3F6C95BFAE3F039819_AdjustorThunk,
	U3CAsyncWaitForRewindU3Ed__11_MoveNext_m5DBC14BB1136B7BCF18066FB833D90CF116C1549_AdjustorThunk,
	U3CAsyncWaitForRewindU3Ed__11_SetStateMachine_mAC2B29B1F4653A143591E2BB2388F8AD9F9A0D64_AdjustorThunk,
	U3CAsyncWaitForKillU3Ed__12_MoveNext_mAA9DB56F9B8B3A6E9069CC690E90634D7F51A276_AdjustorThunk,
	U3CAsyncWaitForKillU3Ed__12_SetStateMachine_m47EB31E97C542A8D7AC82CC7EAF613DC8EEDEABD_AdjustorThunk,
	U3CAsyncWaitForElapsedLoopsU3Ed__13_MoveNext_mD74D8D5D0970E23A40CE0D8B83297BBC68E25712_AdjustorThunk,
	U3CAsyncWaitForElapsedLoopsU3Ed__13_SetStateMachine_m5E02C400A516339612E14DF2ED0B6046D90F6F47_AdjustorThunk,
	U3CAsyncWaitForPositionU3Ed__14_MoveNext_m69E154F3EFA854C85D27C84F5C8624AE7595D01E_AdjustorThunk,
	U3CAsyncWaitForPositionU3Ed__14_SetStateMachine_mA77CBC54370E798D88ACF48F9D68586A41FC741E_AdjustorThunk,
	U3CAsyncWaitForStartU3Ed__15_MoveNext_m0BFB04DC7FE239554F4DF0B3DDD139E42A092F20_AdjustorThunk,
	U3CAsyncWaitForStartU3Ed__15_SetStateMachine_mAD7263100F3F3DBD57295879534C47061409AEB0_AdjustorThunk,
	WaitForCompletion_get_keepWaiting_mCA7642C5A8C37F8C0A2CAE990CE1CB6AEE8FD2D9,
	WaitForCompletion__ctor_m818111A77A3380EE626346FE03A1A604BB896A1A,
	WaitForRewind_get_keepWaiting_mB480319CB28155CA977F94C7FA03EE5353AA1285,
	WaitForRewind__ctor_m66B575E497C363CB5137629B4D6A00D13B7CD5AE,
	WaitForKill_get_keepWaiting_m7979151F1AD842D2E8004FE37A2C51B47AB36647,
	WaitForKill__ctor_m1C9624CE32A1C83CEA14BB5EADD587B6AD79D829,
	WaitForElapsedLoops_get_keepWaiting_m6F7A59CCCC45BBA5125C6FC7AB667CD24359E8F4,
	WaitForElapsedLoops__ctor_m8E720B450DD1350EE81EC3CCB5B6280BE5C51D8B,
	WaitForPosition_get_keepWaiting_m34DFE8356EAFEE916828BFAF4A17A822B47AD687,
	WaitForPosition__ctor_m94DD0A05EF293B8AA83F343A12015C107AF7FDB8,
	WaitForStart_get_keepWaiting_m59700AA1AB726A22C76BFAA0C52FCA460F6E337D,
	WaitForStart__ctor_mD7AB17A603CF22568EEF0D9861C49F6CFD632284,
	DOTweenModuleUtils_Init_mFF2188F42FA7128FE1A9BBC2CB9E6351919C9D15,
	DOTweenModuleUtils_Preserver_mBE8D1FE2AE913FBFFCCF43B5A615D941B3706F43,
	Physics_SetOrientationOnPath_mCC376173A621DA244564EDF8A6347AB2A0F47816,
	Physics_HasRigidbody2D_m86FAA0450979B8AFE6A9EF5E27837387C57765C1,
	Physics_HasRigidbody_m3082D72A658CCDDF6BB1F3D4A3EFBD6397D6E862,
	Physics_CreateDOTweenPathTween_m94D41B65E37500E5ACCF6B6F3B5D53FB031C4245,
};
static const int32_t s_InvokerIndices[651] = 
{
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	10,
	10,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	10,
	32,
	14,
	26,
	14,
	26,
	23,
	23,
	157,
	102,
	23,
	23,
	23,
	23,
	10,
	32,
	14,
	10,
	32,
	14,
	26,
	32,
	14,
	26,
	14,
	26,
	23,
	157,
	27,
	23,
	516,
	23,
	14,
	23,
	23,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	14,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	1696,
	23,
	1696,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	23,
	26,
	26,
	431,
	431,
	35,
	35,
	585,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	14,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	28,
	14,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	23,
	23,
	431,
	431,
	35,
	35,
	585,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	26,
	23,
	26,
	26,
	26,
	26,
	32,
	23,
	23,
	23,
	23,
	32,
	23,
	23,
	32,
	23,
	23,
	14,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	26,
	28,
	28,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	23,
	23,
	14,
	14,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	23,
	56,
	32,
	23,
	102,
	14,
	23,
	14,
	23,
	23,
	28,
	14,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	1526,
	1526,
	1486,
	1503,
	1503,
	186,
	1505,
	186,
	186,
	186,
	186,
	186,
	186,
	186,
	186,
	23,
	656,
	275,
	23,
	656,
	275,
	23,
	656,
	275,
	1540,
	1541,
	1541,
	1541,
	1542,
	1545,
	1546,
	1550,
	1550,
	1551,
	1551,
	23,
	1017,
	23,
	1017,
	23,
	1017,
	23,
	1017,
	23,
	1167,
	23,
	1167,
	23,
	1017,
	23,
	1017,
	1017,
	23,
	23,
	1017,
	23,
	1017,
	1018,
	23,
	1017,
	23,
	1017,
	1018,
	1755,
	1541,
	1541,
	1526,
	1756,
	1550,
	1550,
	23,
	1025,
	23,
	1025,
	23,
	1025,
	23,
	656,
	23,
	1025,
	1040,
	23,
	1025,
	1040,
	23,
	23,
	1017,
	1018,
	23,
	1017,
	1018,
	1527,
	1526,
	1522,
	1527,
	23,
	1001,
	1002,
	23,
	1001,
	1002,
	23,
	1001,
	1002,
	1526,
	1527,
	1526,
	1527,
	1526,
	1526,
	1522,
	1755,
	1755,
	1755,
	1527,
	1526,
	1536,
	1755,
	1541,
	1541,
	1540,
	1541,
	1541,
	1541,
	1755,
	1755,
	1536,
	1526,
	1526,
	1755,
	1756,
	1548,
	1757,
	1756,
	1755,
	1541,
	1541,
	1541,
	1527,
	1758,
	1526,
	1759,
	1527,
	1527,
	1527,
	1760,
	23,
	656,
	275,
	23,
	1001,
	1002,
	23,
	1001,
	1002,
	23,
	1001,
	1002,
	23,
	1001,
	1002,
	23,
	656,
	275,
	23,
	1025,
	1040,
	23,
	1025,
	1040,
	23,
	1025,
	1040,
	23,
	1001,
	1002,
	23,
	1001,
	1002,
	23,
	1025,
	1040,
	23,
	1025,
	1040,
	23,
	1025,
	1040,
	23,
	1025,
	1040,
	23,
	1017,
	1018,
	23,
	1017,
	1018,
	23,
	1017,
	1018,
	23,
	1017,
	1018,
	23,
	1025,
	1040,
	23,
	1025,
	1040,
	23,
	1025,
	1040,
	23,
	1025,
	1040,
	23,
	1025,
	1040,
	23,
	1025,
	1040,
	23,
	1017,
	1018,
	23,
	1017,
	1018,
	23,
	1017,
	1018,
	23,
	1025,
	1040,
	23,
	1025,
	1040,
	23,
	23,
	1025,
	1040,
	23,
	656,
	275,
	23,
	656,
	275,
	23,
	656,
	275,
	23,
	1001,
	1002,
	23,
	10,
	32,
	23,
	1001,
	1002,
	23,
	14,
	26,
	23,
	1001,
	1002,
	23,
	1001,
	1002,
	23,
	1001,
	1002,
	1522,
	1490,
	110,
	110,
	110,
	628,
	1563,
	110,
	1761,
	1761,
	0,
	0,
	0,
	152,
	1519,
	0,
	23,
	1025,
	1040,
	23,
	1025,
	1040,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	102,
	26,
	102,
	26,
	102,
	26,
	102,
	124,
	102,
	833,
	102,
	26,
	3,
	3,
	1636,
	109,
	109,
	1762,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	651,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
